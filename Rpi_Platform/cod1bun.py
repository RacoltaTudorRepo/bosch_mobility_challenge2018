import SerialHandler
import threading,serial,time,sys
from threading import Thread
import SaveEncoder
import cv2


global serialHandler
A=0.0j
B=0.33j
C=0.66j
D=1.0j
timer=2.0

def testBezier(serialHandler):
    #Event test
    ev1=threading.Event()
    serialHandler.readThread.addWaiter("SPLN",ev1)
    time.sleep(2.0)
    print("Start state0")
    sent=serialHandler.sendBezierCurve(A,B,C,D,timer,True)
    if sent:
        ev1.wait()
        print("Confirmed")
        ev1.clear()
        ev1.wait()
        print("Terminated")
    else:
        print("Sending problem")
    print("END_TEST")
    serialHandler.readThread.deleteWaiter("SPLN",ev1)
    time.sleep(0.0)
    
def testStanga(serialHandler):
	pwm=7.0
	e=SaveEncoder.SaveEncoder("EncoderPID%.2f"%pwm+".csv")
	e.open()
  
	ev1=threading.Event()
	ev2=threading.Event()
	serialHandler.readThread.addWaiter("MCTL",ev1,e.save)
	serialHandler.readThread.addWaiter("BRAK",ev1,e.save)
	serialHandler.readThread.addWaiter("ENPB",ev2,e.save)
	time.sleep(0.5)
	print("Start moving") #pornire thread
	sent=serialHandler.sendMove(pwm,-20.0) # < e intre -23 23
	if sent:
		ev1.wait()
		print("Confirmed")
        
	else:
		print("Sending  problem")
	time.sleep(2.0) #cat functioneaza
	ev1.clear()
	print("Start braking")
	sent=serialHandler.sendBrake(0.5) # nu da 0 niciodata
	if sent:
		ev1.wait()
		print("Confirmed")
        
	else:
		print("Sending problem")
    
	time.sleep(1.0)
	print("END_TEST")	
	serialHandler.readThread.deleteWaiter("BRAK",ev1)
	serialHandler.readThread.deleteWaiter("MCTL",ev1)
	serialHandler.readThread.deleteWaiter("ENPB",ev2)
	e.close()
	
def testDreapta(serialHandler):
	pwm=7.0
	e=SaveEncoder.SaveEncoder("EncoderPID%.2f"%pwm+".csv")
	e.open()
  
	ev1=threading.Event()
	ev2=threading.Event()
	serialHandler.readThread.addWaiter("MCTL",ev1,e.save)
	serialHandler.readThread.addWaiter("BRAK",ev1,e.save)
	serialHandler.readThread.addWaiter("ENPB",ev2,e.save)
	time.sleep(0.5)
	print("Start moving") #pornire thread
	sent=serialHandler.sendMove(pwm, 20.0) # < e intre -23 23
	if sent:
		ev1.wait()
		print("Confirmed")
        
	else:
		print("Sending  problem")
	time.sleep(2.0) #cat functioneaza
	ev1.clear()
	print("Start braking")
	sent=serialHandler.sendBrake(0.5) # nu da 0 niciodata
	if sent:
		ev1.wait()
		print("Confirmed")
        
	else:
		print("Sending problem")
    
	time.sleep(1.0)
	print("END_TEST")	
	serialHandler.readThread.deleteWaiter("BRAK",ev1)
	serialHandler.readThread.deleteWaiter("MCTL",ev1)
	serialHandler.readThread.deleteWaiter("ENPB",ev2)
	e.close()    
    
    
def testMOVEAndBrake(serialHandler):
    #Event test
    pwm=7.0 #7-15 ptr PWM
    e=SaveEncoder.SaveEncoder("EncoderPID%.2f"%pwm+".csv")
    e.open()
    
    ev1=threading.Event()
    ev2=threading.Event()
    serialHandler.readThread.addWaiter("MCTL",ev1,e.save)
    serialHandler.readThread.addWaiter("BRAK",ev1,e.save)
    serialHandler.readThread.addWaiter("ENPB",ev2,e.save)

    time.sleep(1.0)
    print("Start moving") #pornire thread
    sent=serialHandler.sendMove(pwm,13.0) # < e intre -23 23
    if sent:
        ev1.wait()
        print("Confirmed")
        
    else:
        print("Sending  problem")
    time.sleep(2.0) #cat functioneaza
    ev1.clear()
    
    #adaugat de mine !
    sent=serialHandler.sendMove(pwm,-13.0) # < e intre -23 23
    if sent:
        ev1.wait()
        print("Confirmed")
        
    else:
        print("Sending  problem")
    time.sleep(2.0) #cat functioneaza
    ev1.clear()
    
    #final adaugat
    print("Start braking")
    sent=serialHandler.sendBrake(0.5) # nu da 0 niciodata
    if sent:
        ev1.wait()
        print("Confirmed")
        
    else:
        print("Sending problem")
    
    time.sleep(1.0)
    print("END_TEST")
    serialHandler.readThread.deleteWaiter("BRAK",ev1)
    serialHandler.readThread.deleteWaiter("MCTL",ev1)
    serialHandler.readThread.deleteWaiter("ENPB",ev2)
    e.close()
    
def testBrake(serialHandler):
    #Event test
    ev1=threading.Event()
    serialHandler.readThread.addWaiter("BRAK",ev1)
    time.sleep(2.0)


    print("Start moving")
    for i in range(0,1):
        if i%2==0:
            sent=serialHandler.sendBrake(0.0)
        else:
            sent=serialHandler.sendBrake(0.0)
        if sent:
            ev1.wait()
            print("Confirmed")
            
        else:
            print("Sending problem")
        time.sleep(2)
        ev1.clear()
    
    time.sleep(1.5)
    # sent=serialHandler.sendBrake(0.0)
    if sent:
            ev1.wait()
            print("Confirmed")
    serialHandler.readThread.deleteWaiter("MCTL",ev1)
    ev1.close()
    
def testPid(SerialHandler):
    ev1=threading.Event()
    serialHandler.readThread.addWaiter("PIDA",ev1)
    sent=serialHandler.sendPidActivation(True)
    if sent:
        ev1.wait()
        print("Confirmed")
        
    else:
        print("Sending problem")
    serialHandler.readThread.deleteWaiter("PIDA",ev1)
def testSafetyStop(SerialHandler):
    ev1=threading.Event()
    serialHandler.readThread.addWaiter("SFBR",ev1)
    sent=serialHandler.sendSafetyStopActivation(False)
    if sent:
        ev1.wait()
        print("Confirmed")
        
    else:
        print("Sending problem")
    serialHandler.readThread.deleteWaiter("SFBR",ev1)
def testPidValue(SerialHandler):
    ev1=threading.Event()
    serialHandler.readThread.addWaiter("PIDS",ev1)
    # (kp,ki,kd,tf)=(2.8184,7.0832,0.28036,0)
    (kp,ki,kd,tf)=(0.93143,2.8,0.0,0.0001)
    sent=serialHandler.sendPidValue(kp,ki,kd,tf)
    if sent:
        ev1.wait()
        print("Confirmed")
        
    else:
        print("Sending problem")
    serialHandler.readThread.deleteWaiter("PIDS",ev1)
def videoluam():
	cap = cv2.VideoCapture(0)
	print (cap.isOpened())
	while True:
		ret, img = cap.read()
		frame = img
		cv2.imshow('frame',frame)
		if cv2.waitKey(1) & 0xFF == ord('q'):
			break
			
	cap.release()
	cv2.destroyAllWindows()
def main():
    #Initiliazation
	global serialHandler
	t1=Thread(target=videoluam)
	t1.start()
	serialHandler=SerialHandler.SerialHandler()
	serialHandler.startReadThread()
        #testBezier(serialHandler)
	testStanga(serialHandler)
	testDreapta(serialHandler)
	testBrake(serialHandler)
	#testMOVEAndBrake(serialHandler)
	time.sleep(0.5)
	serialHandler.close()
	t1.join()
	print ("Tsdgdfz")

if __name__=="__main__":
    main()
        
