import SerialHandler
import threading,serial,time,sys
from threading import Thread
import SaveEncoder
import cv2
#import Graph
from ControlSteerAngle import Line
import CarClient

global serialHandler
A=-0.9+ 0.0j
B=1.35 + 0.45j
C=1.8 + 0.9j
D=1.8 + 1.35j
timer=2.0

def testBezier(serialHandler):
    #Event test
    ev1=threading.Event()
    serialHandler.readThread.addWaiter("SPLN",ev1)
    time.sleep(2.0)
    print("Start state0")
    sent=serialHandler.sendBezierCurve(A,B,C,D,timer,True)
    if sent:
        ev1.wait()
        print("Confirmed")
        ev1.clear()
        ev1.wait()
        print("Terminated")
    else:
        print("Sending problem")
    print("END_TEST")
    serialHandler.readThread.deleteWaiter("SPLN",ev1)
    time.sleep(0.0)
    
def testStanga(serialHandler):
	pwm=9.0
	e=SaveEncoder.SaveEncoder("EncoderPID%.2f"%pwm+".csv")
	e.open()
  
	ev1=threading.Event()
	ev2=threading.Event()
	serialHandler.readThread.addWaiter("MCTL",ev1,e.save)
	serialHandler.readThread.addWaiter("BRAK",ev1,e.save)
	serialHandler.readThread.addWaiter("ENPB",ev2,e.save)
	print("Start moving") #pornire thread
	sent=serialHandler.sendMove(pwm,-19.0) # < e intre -23 23
	if sent:
		ev1.wait()
		print("Confirmed")
        
	else:
		print("Sending  problem")
	time.sleep(1.7) #cat functioneaza
	ev1.clear()
	print("Start braking")
	sent=serialHandler.sendBrake(0.5) #centrare servo pe 0.5 !!
	if sent:
		ev1.wait()
		print("Confirmed")
        
	else:
		print("Sending problem")
    
	print("END_TEST")	
	serialHandler.readThread.deleteWaiter("BRAK",ev1)
	serialHandler.readThread.deleteWaiter("MCTL",ev1)
	serialHandler.readThread.deleteWaiter("ENPB",ev2)
	e.close()
	
def testDreapta(serialHandler):
	pwm=9.0
	e=SaveEncoder.SaveEncoder("EncoderPID%.2f"%pwm+".csv")
	e.open()
  
	ev1=threading.Event()
	ev2=threading.Event()
	serialHandler.readThread.addWaiter("MCTL",ev1,e.save)
	serialHandler.readThread.addWaiter("BRAK",ev1,e.save)
	serialHandler.readThread.addWaiter("ENPB",ev2,e.save)
	print("Start moving") #pornire thread
	sent=serialHandler.sendMove(pwm, 20.0) # < e intre -23 23
	if sent:
		ev1.wait()
		print("Confirmed")
        
	else:
		print("Sending  problem")
	time.sleep(1.0) #cat functioneaza
	ev1.clear()
	print("Start braking")
	sent=serialHandler.sendBrake(0.5) # nu da 0 niciodata
	if sent:
		ev1.wait()
		print("Confirmed")
        
	else:
		print("Sending problem")
    
	print("END_TEST")	
	serialHandler.readThread.deleteWaiter("BRAK",ev1)
	serialHandler.readThread.deleteWaiter("MCTL",ev1)
	serialHandler.readThread.deleteWaiter("ENPB",ev2)
	sent=serialHandler.sendMove(pwm, 0.5)
	e.close()    
    
    
def testMOVEAndBrake(serialHandler):
    #Event test
    pwm=7.0 #7-15 ptr PWM
    e=SaveEncoder.SaveEncoder("EncoderPID%.2f"%pwm+".csv")
    e.open()
    
    ev1=threading.Event()
    ev2=threading.Event()
    serialHandler.readThread.addWaiter("MCTL",ev1,e.save)
    serialHandler.readThread.addWaiter("BRAK",ev1,e.save)
    serialHandler.readThread.addWaiter("ENPB",ev2,e.save)

    time.sleep(1.0)
    print("Start moving") #pornire thread
    sent=serialHandler.sendMove(pwm,13.0) # < e intre -23 23
    if sent:
        ev1.wait()
        print("Confirmed")
        
    else:
        print("Sending  problem")
    time.sleep(2.0) #cat functioneaza
    ev1.clear()
    
    #adaugat de mine !
    sent=serialHandler.sendMove(pwm,-13.0) # < e intre -23 23
    if sent:
        ev1.wait()
        print("Confirmed")
        
    else:
        print("Sending  problem")
    time.sleep(2.0) #cat functioneaza
    ev1.clear()
    
    #final adaugat
    print("Start braking")
    sent=serialHandler.sendBrake(0.5) # nu da 0 niciodata
    if sent:
        ev1.wait()
        print("Confirmed")
        
    else:
        print("Sending problem")
    
    time.sleep(1.0)
    print("END_TEST")
    serialHandler.readThread.deleteWaiter("BRAK",ev1)
    serialHandler.readThread.deleteWaiter("MCTL",ev1)
    serialHandler.readThread.deleteWaiter("ENPB",ev2)
    e.close()
    
def testBrake(serialHandler, a):
    #Event test
    ev1=threading.Event()
    serialHandler.readThread.addWaiter("BRAK",ev1)
    time.sleep(2.0)


    print("Start moving")
    for i in range(0,2):
        if i%2==0:
            sent=serialHandler.sendBrake(-20.0)
        else:
            sent=serialHandler.sendBrake(20.0)
        if sent:
            ev1.wait()
            print("Confirmed")
        else:
            print("Sending problem")
        time.sleep(2)
        ev1.clear()
    
    time.sleep(1.5)
    sent=serialHandler.sendBrake(0.0)
    if sent:
            ev1.wait()
            print("Confirmed")
    serialHandler.readThread.deleteWaiter("MCTL",ev1)
    
    
def testPid(SerialHandler):
    ev1=threading.Event()
    serialHandler.readThread.addWaiter("PIDA",ev1)
    sent=serialHandler.sendPidActivation(True)
    if sent:
        ev1.wait()
        print("Confirmed")
        
    else:
        print("Sending problem")
    serialHandler.readThread.deleteWaiter("PIDA",ev1)
def testSafetyStop(SerialHandler):
    ev1=threading.Event()
    serialHandler.readThread.addWaiter("SFBR",ev1)
    sent=serialHandler.sendSafetyStopActivation(False)
    if sent:
        ev1.wait()
        print("Confirmed")
        
    else:
        print("Sending problem")
    serialHandler.readThread.deleteWaiter("SFBR",ev1)
def testPidValue(SerialHandler):
    ev1=threading.Event()
    serialHandler.readThread.addWaiter("PIDS",ev1)
    # (kp,ki,kd,tf)=(2.8184,7.0832,0.28036,0)
    (kp,ki,kd,tf)=(0.93143,2.8,0.0,0.0001)
    sent=serialHandler.sendPidValue(kp,ki,kd,tf)
    if sent:
        ev1.wait()
        print("Confirmed")
        
    else:
        print("Sending problem")
    serialHandler.readThread.deleteWaiter("PIDS",ev1)
def videoluam():
	cap = cv2.VideoCapture(0)
	print (cap.isOpened())
	while True:
		ret, img = cap.read()
		frame = img
		cv2.imshow('frame',frame)
		if cv2.waitKey(1) & 0xFF == ord('q'):
			break
			
	cap.release()
	cv2.destroyAllWindows()

def inainte():
    print('in inainte')
    pwm=11.0 #7-15 ptr PWM
    e=SaveEncoder.SaveEncoder("EncoderPID%.2f"%pwm+".csv")
    e.open()

    ev1=threading.Event()
    ev2=threading.Event()
    serialHandler.readThread.addWaiter("MCTL",ev1,e.save)
    serialHandler.readThread.addWaiter("BRAK",ev1,e.save)
    serialHandler.readThread.addWaiter("ENPB",ev2,e.save)

    #time.sleep(0.1)
    print("Start moving") #pornire thread
    sent=serialHandler.sendMove(pwm,0.5) # < e intre -23 23
    if sent:
        ev1.wait()
        print("Confirmed") 
    else:
        print("Sending  problem")
    time.sleep(0.8) #cat functioneaza
    ev1.clear()    
    #final adaugat
    print("Start braking")
    #sent=serialHandler.sendBrake(0.5) # nu da 0 niciodata
    #if sent:
     #   ev1.wait()
      #  print("Confirmed")   
    #else:
      #  print("Sending problem")

    #time.sleep(0.0)
    print("END_TEST")
    serialHandler.readThread.deleteWaiter("BRAK",ev1)
    serialHandler.readThread.deleteWaiter("MCTL",ev1)
    serialHandler.readThread.deleteWaiter("ENPB",ev2)
    e.close()

def frana(serialHandler):
    #Event test
    ev1=threading.Event()
    serialHandler.readThread.addWaiter("BRAK",ev1)
    #time.sleep(2.0)


    print("Start moving")
  
    sent=serialHandler.sendBrake(0.5)

    if sent:
        ev1.wait()
        print("Confirmed")
    else:
        print("Sending problem")
        ev1.clear() 

    serialHandler.readThread.deleteWaiter("MCTL",ev1)
    
def testLStanga(serialHandler):
    frana(serialHandler)
    
def testLDreapta(serialHandler):
    time.sleep(1.0)

def inainteAngle(serialHandler, angle):
    print('in inainte')
    pwm=7.5 #7-15 ptr PWM
    if angle == 0.0:
        pwm = 8.0
    else:
        pwm = 8.0

    e=SaveEncoder.SaveEncoder("EncoderPID%.2f"%pwm+".csv")
    e.open()

    ev1=threading.Event()
    ev2=threading.Event()
    serialHandler.readThread.addWaiter("MCTL",ev1,e.save)
    serialHandler.readThread.addWaiter("BRAK",ev1,e.save)
    serialHandler.readThread.addWaiter("ENPB",ev2,e.save)

    #time.sleep(0.1)
    #print("Start moving") #pornire thread
    sent=serialHandler.sendMove(pwm,angle) # < e intre -23 23
    if sent:
        ev1.wait()
        #print("Confirmed") 
    else:
        print("Sending  problem")
    time.sleep(0.7) #cat functioneaza
    ev1.clear()    
    #final adaugat
    #print("Start braking")
    #sent=serialHandler.sendBrake(0.0) # nu da 0 niciodata
    #if sent:
    #    ev1.wait()
    #    print("Confirmed")   
    #else:
    #    print("Sending problem")

    #time.sleep(0.0)
    print("END_TEST")
    serialHandler.readThread.deleteWaiter("BRAK",ev1)
    serialHandler.readThread.deleteWaiter("MCTL",ev1)
    serialHandler.readThread.deleteWaiter("ENPB",ev2)
    e.close()



def main():
    #Initiliazation
    global serialHandler
    #t1=Thread(target=videoluam)
    #t1.start()
    serialHandler=SerialHandler.SerialHandler()
    serialHandler.startReadThread()
    '''graf = Graph.Graph('grafTest.txt')
    graf.showGraph()
    solution = graf.getDirections([0.9, 0.0], [0.9, 3.15]) #punct start
    print(solution)
    serialHandler=SerialHandler.SerialHandler()
    serialHandler.startReadThread()
    for i in solution:
        if(i=='A'):
            inainte()
        elif(i=='L'):
            testStanga(serialHandler)
        elif(i=='R'):
            testDreapta(serialHandler)
        elif(i=='LongL'):
            testStanga(serialHandler)
        elif(i=='LongR'):
            testLDreapta(serialHandler)'''
    inainteAngle(serialHandler, 0)
    frana(serialHandler)
    print(123)
    serialHandler.close()

def main1():
    global serialHandler
    serialHandler=SerialHandler.SerialHandler()
    serialHandler.startReadThread()
    
    linePointStart = 0.0 + 0.0j
    linePointEnd = 4.0 + 0.0j
    line = Line(linePointStart, linePointEnd)

    carInfo = CarClient.CarInfo()
    carInfo.start()
    (carPoz, carOr) = carInfo.getInfo()

    while (carPoz != linePointEnd):
        steeringAngle = line.getSteerAngle(carPoz, carOr)
        inainteAngle(serialHandler, steeringAngle)
        (carPoz, carOr) = carInfo.getInfo()
    frana(serialHandler)
    serialHandler.close()
    
if __name__=="__main__":
    main1()
    
        
