from threading import Thread
from time import sleep
import CarClient
import math
from ControlSteerAngle import Line


import SerialHandler
import SerialHandlerTest

from Graph import Graph

#orientarea relativa la sistem (rotim orientarea cu +90)
# da doar liste [x, y]
def getNodes(currentPoint, orientation, destination):
        graf = Graph('grafTest.txt')
        closestPoint = graf.getClosestPointToGraph(currentPoint, orientation)
        print(closestPoint)
        path = graf.UniformCostSearch(closestPoint, destination)
        if path == None:
                print('Esti prost ma. ai dat start de pe o banda si end pe alta la functia getNodes')
                return None
        path.insert(0, currentPoint)

        return path

def isCloseEnough(point1, point2):
        epsilon = 0.45 / 0.9
        
        distance = math.sqrt((point1[0] - point2[0]) * (point1[0] - point2[0]) + (point1[1] - point2[1]) * (point1[1] - point2[1]))
        print('Distanta la punct', distance)
        if distance <= epsilon:
                return True
        else:
                return False
        
def test1():
        serialHandler=SerialHandler.SerialHandler()
        serialHandler.startReadThread()

        SerialHandlerTest.frana(serialHandler)
        serialHandler.close()
        exit()
        

        carInfo = CarClient.CarInfo()
        carInfo.start()
        print(1234)
        (position, orientation) = carInfo.getInfo()
        #print(position)
        destination = [2.25, 2.25]
        #print(orientation)
        points = getNodes(position, orientation, destination)
        if points == None:
                exit()
        print(points)
        #SerialHandlerTest.frana(serialHandler)
        #serialHandler.close()
        #return
        indexStart = 0
        indexEnd = 1
        #orientarea : x + y * j   x- cos y-sin !!!!!!!!!!
        
        while isCloseEnough(position, destination) == False:
            if (indexStart >= points.__len__()):
                print('Ai ratat punctul final')
                break;
            pointStart = points[indexStart]
            pointEnd = points[indexEnd]
            indexStart += 1
            indexEnd += 1
            
            line = Line(pointStart, pointEnd)

            while isCloseEnough(position, pointEnd) == False:
                    angle = line.getSteerAngle(position, orientation)
                    # misca masina dupa unghi
                    SerialHandlerTest.inainteAngle(serialHandler, angle)
                    SerialHandlerTest.frana(serialHandler)
                    (position, orientation) = carInfo.getInfo() # complexe               
                    print("Punct in care vreau sa ajung", pointEnd)    
        SerialHandlerTest.frana(serialHandler)
        serialHandler.close()

def test2():
        serialHandler=SerialHandler.SerialHandler()
        serialHandler.startReadThread()
        
        pointComplex = [1.0, 2.0]
        orientation = [1.0, 0.0]
        line = Line([0.0, 0.0], [4.0, 0.0])
        
        angle = line.getSteerAngle(pointComplex, orientation)
        SerialHandlerTest.inainteAngle(serialHandler, angle)
        sleep(3)
        SerialHandlerTest.frana(serialHandler)

if __name__ == "__main__":
        test1()
