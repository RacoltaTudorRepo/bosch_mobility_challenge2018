import math

class Line():
    def __init__(self, pointStart, pointEnd):
        self.pointStart = pointStart
        self.pointEnd = pointEnd
        self.A = self.__getA()
        self.B = self.__getB()
        self.C = self.__getC()
        #print(self.A, self.B, self.C)

    def __getA(self):
        return self.pointStart[1] - self.pointEnd[1]

    def __getB(self):
        return self.pointEnd[0] - self.pointStart[0]

    def __getC(self):
        return self.pointStart[0] * self.pointEnd[1] - self.pointEnd[0] * self.pointStart[1]

    def __getPointValue(self, point):
        return self.A * point[0] + self.B * point[1] + self.C

    # deasupra 1, pe ea 0, sub ea -1, se uita la orientarea liniei
    def getPozPointToLine(self, point):
        value = self.__getPointValue(point)
        if (value == 0):
            print('Pe linie'); # esti pe linie
            return (value, 0)
        #print(value)

        if (self.B == 0):
            #linie e perpendiculara pe Ox
            x = -self.C / self.A
            
            # fara orientare
            if point[0] < x: # sub line
                if self.pointStart[1] > self.pointEnd[1]:
                    #print('Sub linie')
                    return (value, -1)
                else:
                    #print('Deasupra lini')
                    return (value, 1)
            else:
                if self.pointStart[1] > self.pointEnd[1]:
                    #print('Deasupra linie')
                    return (value, 1)
                else:
                    #print('Sub linie')
                    return (value, -1)
        
        #test deasupra fara orientare
        if (value > 0 and self.B > 0) or (value < 0 and self.B < 0):
            #test cu orientare linie
            if (self.pointStart[0] <= self.pointEnd[0] and
                self.pointStart[1] <= self.pointEnd[1]):
                #print('Deasupra linie') # ii deasupra
                return (value, 1)
            else:
                #print('Sub linie') #ii dedesupt
                return (value, -1)
        else: #suntem dedesupt
            if (self.pointStart[0] <= self.pointEnd[0] and
                self.pointStart[1] <= self.pointEnd[1]):
                #print('Sub linie') # ii dedesupt
                return (value, -1)
            else:
                #print('Deasupra linie') # ii deasupra
                return (value, 1)

    def getAngle(self, value, position, orientation):

        valuePoz = math.fabs(value)
        distance = valuePoz / math.sqrt(self.A * self.A + self.B * self.B)
        distanceRead = 45 * distance / 0.45
        
        
        if (position == 0): # pe linie
            return 0
        if (position == 1): # deasupra
            return distanceRead/5
        if (position == -1): # dedesupt
            return -distanceRead/5

    # determina daca orientarea masinii este spre linie sau nu
    def gettingCloser2Line(self, point, orientation, valuePoint):
        point2 = (point[0] + orientation[0], point[1] + orientation[1])
        valuePoint2 = self.__getPointValue(point2)
        valuePoint2P = math.fabs(valuePoint2)

        #print(valuePoint, valuePoint2P)

        if valuePoint2P > valuePoint:
            return False # ma indepartez
        else:
            return True # ma aproprii

    
    def getAngleWithVector(self, vector):
        lineVector = (self.pointEnd[0] - self.pointStart[0], self.pointEnd[1] - self.pointStart[1])
        lineVectorModule = math.sqrt(lineVector[0] * lineVector[0] + lineVector[1] * lineVector[1])
        lineVectorNorm = (lineVector[0] / lineVectorModule, lineVector[1] / lineVectorModule)

        vectorModule = math.sqrt(vector[0] * vector[0] + vector[1] * vector[1])
        vectorNorm = (vector[0] / vectorModule, vector[1] / vectorModule)
        #print(vectorNorm, lineVectorNorm)
        cosAngle = vectorNorm[0] * lineVectorNorm[0] + vectorNorm[1] * lineVectorNorm[1];
        cosAngle = math.fabs(cosAngle)
        return math.degrees(math.acos(cosAngle))

    def getAngle2(self, valuePos, positionToLine, position, orientation):
        valuePosP = math.fabs(valuePos)
        distanceToLine = valuePosP / math.sqrt(self.A * self.A + self.B * self.B)

        distanceReal = 45 * distanceToLine / 0.45
        closer = self.gettingCloser2Line(position, orientation, valuePosP)
        angleWithLine = self.getAngleWithVector(orientation)
        
        print(angleWithLine)
        if closer == False: # ma indepartez virez maxim
            #print('Ma indepartez')
            if positionToLine == 1: # ma departez si sunt deasupra
                return 22.0
            else: # ma departez si sunt sub linie
                return -22.0 
        else:
            #print('Ma apropii')
            #print('Distanta ', distanceReal)
            #print('Unghi', angleWithLine)
            #print('Distanta plan ', distanceToLine)
            #varianta andrei
            #steer = 90.0 - 2 * angleWithLine #+ distanceToLine
            #steer = 22.0 - angleWithLine + distanceToLine
            steer = 45.0 - angleWithLine + distanceToLine
            if steer < 0.0:
                steer = 0.0
            if positionToLine == 1: # ma apropii si sunt deasupra
                return steer
            else: # ma apropii si sunt dedesupt
                return -steer
        

    def getSteerAngle(self, point, orientation):
        (valuePoint, relativePosition) = self.getPozPointToLine(point)
        #steerAngle = self.getAngle(valuePoint, relativePosition, orientation)
        steerAngle = self.getAngle2(valuePoint, relativePosition, point, orientation)
        print('Unghi', steerAngle)
        return steerAngle
        

if __name__ == "__main__":
    pointStart = [1.0, 2.0]
    pointEnd = [8.0, 2.0]
    line = Line(pointStart,pointEnd)
    point = [4.0, 4.0]
    print(line.getSteerAngle(point, [1.0, 1.0]))
    print(line.getSteerAngle(point, [-1.0, -1.0]))
