import SerialHandler
import threading,serial,time,sys
from threading import Thread
import SaveEncoder
import cv2
import numpy as np


global serialHandler
A=0.0j
B=0.33j
C=0.66j
D=1.0j
timer=2.0

def testBezier(serialHandler):
    #Event test
    ev1=threading.Event()
    serialHandler.readThread.addWaiter("SPLN",ev1)
    time.sleep(2.0)
    print("Start state0")
    sent=serialHandler.sendBezierCurve(A,B,C,D,timer,True)
    if sent:
        ev1.wait()
        print("Confirmed")
        ev1.clear()
        ev1.wait()
        print("Terminated")
    else:
        print("Sending problem")
    print("END_TEST")
    serialHandler.readThread.deleteWaiter("SPLN",ev1)
    time.sleep(0.0)
    
def testStanga(serialHandler):
    pwm=7.0
    e=SaveEncoder.SaveEncoder("EncoderPID%.2f"%pwm+".csv")
    e.open()
  
    ev1=threading.Event()
    ev2=threading.Event()
    serialHandler.readThread.addWaiter("MCTL",ev1,e.save)
    serialHandler.readThread.addWaiter("BRAK",ev1,e.save)
    serialHandler.readThread.addWaiter("ENPB",ev2,e.save)
    time.sleep(0.5)
    print("Start moving") #pornire thread
    sent=serialHandler.sendMove(pwm,-20.0) # < e intre -23 23
    if sent:
        ev1.wait()
        print("Confirmed")
        
    else:
        print("Sending  problem")
    time.sleep(2.0) #cat functioneaza
    ev1.clear()
    print("Start braking")
    sent=serialHandler.sendBrake(0.5) # nu da 0 niciodata
    if sent:
        ev1.wait()
        print("Confirmed")
        
    else:
        print("Sending problem")
    
    time.sleep(1.0)
    print("END_TEST")   
    serialHandler.readThread.deleteWaiter("BRAK",ev1)
    serialHandler.readThread.deleteWaiter("MCTL",ev1)
    serialHandler.readThread.deleteWaiter("ENPB",ev2)
    e.close()
    
def testDreapta(serialHandler):
    pwm=7.0
    e=SaveEncoder.SaveEncoder("EncoderPID%.2f"%pwm+".csv")
    e.open()
  
    ev1=threading.Event()
    ev2=threading.Event()
    serialHandler.readThread.addWaiter("MCTL",ev1,e.save)
    serialHandler.readThread.addWaiter("BRAK",ev1,e.save)
    serialHandler.readThread.addWaiter("ENPB",ev2,e.save)
    time.sleep(0.5)
    print("Start moving") #pornire thread
    sent=serialHandler.sendMove(pwm, 20.0) # < e intre -23 23
    if sent:
        ev1.wait()
        print("Confirmed")
        
    else:
        print("Sending  problem")
    time.sleep(2.0) #cat functioneaza
    ev1.clear()
    print("Start braking")
    sent=serialHandler.sendBrake(0.5) # nu da 0 niciodata
    if sent:
        ev1.wait()
        print("Confirmed")
        
    else:
        print("Sending problem")
    
    time.sleep(1.0)
    print("END_TEST")   
    serialHandler.readThread.deleteWaiter("BRAK",ev1)
    serialHandler.readThread.deleteWaiter("MCTL",ev1)
    serialHandler.readThread.deleteWaiter("ENPB",ev2)
    e.close()    
    
    
def testMOVEAndBrake(serialHandler):
    #Event test
    pwm=7.0 #7-15 ptr PWM
    e=SaveEncoder.SaveEncoder("EncoderPID%.2f"%pwm+".csv")
    e.open()
    
    ev1=threading.Event()
    ev2=threading.Event()
    serialHandler.readThread.addWaiter("MCTL",ev1,e.save)
    serialHandler.readThread.addWaiter("BRAK",ev1,e.save)
    serialHandler.readThread.addWaiter("ENPB",ev2,e.save)

    time.sleep(1.0)
    print("Start moving") #pornire thread
    sent=serialHandler.sendMove(pwm,13.0) # < e intre -23 23
    if sent:
        ev1.wait()
        print("Confirmed")
        
    else:
        print("Sending  problem")
    time.sleep(2.0) #cat functioneaza
    ev1.clear()
    
    #adaugat de mine !
    sent=serialHandler.sendMove(pwm,-13.0) # < e intre -23 23
    if sent:
        ev1.wait()
        print("Confirmed")
        
    else:
        print("Sending  problem")
    time.sleep(2.0) #cat functioneaza
    ev1.clear()
    
    #final adaugat
    print("Start braking")
    sent=serialHandler.sendBrake(0.5) # nu da 0 niciodata
    if sent:
        ev1.wait()
        print("Confirmed")
        
    else:
        print("Sending problem")
    
    time.sleep(1.0)
    print("END_TEST")
    serialHandler.readThread.deleteWaiter("BRAK",ev1)
    serialHandler.readThread.deleteWaiter("MCTL",ev1)
    serialHandler.readThread.deleteWaiter("ENPB",ev2)
    e.close()
    
def testBrake(serialHandler):
    #Event test
    ev1=threading.Event()
    serialHandler.readThread.addWaiter("BRAK",ev1)
    time.sleep(2.0)


    print("Start moving")
    for i in range(0,1):
        if i%2==0:
            sent=serialHandler.sendBrake(0.0)
        else:
            sent=serialHandler.sendBrake(0.0)
        if sent:
            ev1.wait()
            print("Confirmed")
            
        else:
            print("Sending problem")
        time.sleep(2)
        ev1.clear()
    
    time.sleep(1.5)
    # sent=serialHandler.sendBrake(0.0)
    if sent:
            ev1.wait()
            print("Confirmed")
    serialHandler.readThread.deleteWaiter("MCTL",ev1)
    ev1.close()
    
def testPid(SerialHandler):
    ev1=threading.Event()
    serialHandler.readThread.addWaiter("PIDA",ev1)
    sent=serialHandler.sendPidActivation(True)
    if sent:
        ev1.wait()
        print("Confirmed")
        
    else:
        print("Sending problem")
    serialHandler.readThread.deleteWaiter("PIDA",ev1)
    
def testSafetyStop(SerialHandler):
    ev1=threading.Event()
    serialHandler.readThread.addWaiter("SFBR",ev1)
    sent=serialHandler.sendSafetyStopActivation(False)
    if sent:
        ev1.wait()
        print("Confirmed")
        
    else:
        print("Sending problem")
    serialHandler.readThread.deleteWaiter("SFBR",ev1)
    
def testPidValue(SerialHandler):
    ev1=threading.Event()
    serialHandler.readThread.addWaiter("PIDS",ev1)
    # (kp,ki,kd,tf)=(2.8184,7.0832,0.28036,0)
    (kp,ki,kd,tf)=(0.93143,2.8,0.0,0.0001)
    sent=serialHandler.sendPidValue(kp,ki,kd,tf)
    if sent:
        ev1.wait()
        print("Confirmed")
        
    else:
        print("Sending problem")
    serialHandler.readThread.deleteWaiter("PIDS",ev1)
    
def videoluam():
    cap = cv2.VideoCapture(0)
    cap.set(3, 300)
    cap.set(4, 300) 
    print (cap.isOpened())
    while True:
            ret, img = cap.read()
            if img is not None:
                frame = img    
                crop_img = frame[100:300, 0:300]
                gray=cv2.cvtColor(crop_img,cv2.COLOR_BGR2GRAY)
                blur=cv2.GaussianBlur(gray,(5,5),0)
                ret, thresh= cv2.threshold(blur,60,255,cv2.THRESH_BINARY_INV)
                _, contours,hierarchy = cv2.findContours(thresh.copy(), 1, cv2.CHAIN_APPROX_NONE)
                # Find the biggest contour (if detected)
                if len(contours) > 0:
                    c = max(contours, key=cv2.contourArea)
                    M = cv2.moments(c)
                    cx = int(M['m10']/M['m00'])
                    cy = int(M['m01']/M['m00'])
                    cv2.line(crop_img,(cx,0),(cx,720),(255,0,0),1)
                    cv2.line(crop_img,(0,cy),(1280,cy),(255,0,0),1)
                    cv2.drawContours(crop_img, contours, -1, (0,255,0), 1)
                    if cx >= 120:
                        print ("Turn Left!")
                    if cx < 120 and cx > 50:
                        print ("On Track!")
                    if cx <= 50:
                        print ("Turn Right")
                    else:
                        print("I don't see the line")
                cv2.imshow('frame',frame)
                cv2.imshow('gray',gray)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
                cap.release()
                cv2.destroyAllWindows()
    
def main():
    #Initiliazation
    global serialHandler
    t1=Thread(target=videoluam)
    t1.start()
    serialHandler=SerialHandler.SerialHandler()
    serialHandler.startReadThread()
        #testBezier(serialHandler)
    #testStanga(serialHandler)
    #testDreapta(serialHandler)
    #testBrake(serialHandler)
    #testMOVEAndBrake(serialHandler)
    time.sleep(0.5)
    serialHandler.close()

if __name__=="__main__":
    main()
        
