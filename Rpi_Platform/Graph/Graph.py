import json
import matplotlib.pyplot as plt
import Node
import PriorityQueue
import GraphDirection

# printGraph, showGraph and getDirections should by use

class Graph:

    def __init__(self, graphFile):
        f = open(graphFile, 'r')
        mp = json.load(f)

        listNodes = []

        for key in mp.keys():
            nodeList = mp[key]
            for el in nodeList:
                name = el['NAME']
                listNeighb = []
                listNeighb.append(el['OUT_BACK'])
                listNeighb.append(el['OUT_RIGHT'])
                listNeighb.append(el['OUT_AHEAD'])
                listNeighb.append(el['OUT_LEFT'])
                coordinates = el['COORDINATES']

                graphNode = Node.StorageNode(name, listNeighb, coordinates)
                listNodes.append(graphNode)

        for node in listNodes:
            newNeighb = []
            for neighbor in node.neighbors:
                neighbor = self.__getNodeFromList(listNodes, neighbor)
                if neighbor != None:
                    newNeighb.append(neighbor)
            node.neighbors = newNeighb

        self.__nodes = listNodes

    # show a text representation
    def printGraph(self):
        for node in self.__nodes:
           print(node.string())

    # show a graphic representation
    def showGraph(self):
        listX = []
        listY = []

        for node in self.__nodes:
            listX.append(node.coordinates[0])
            listY.append(node.coordinates[1])
            for neighbor in node.neighbors:
                self.__addLinePlot(plt, node.coordinates, neighbor.coordinates)

        plt.plot(listX, listY, 'ro')
        plt.show()

    # return list of directions
    def getDirections(self, coordinate1, coordinate2):
        nodeList = self.__UniformCostSearch(coordinateStart=coordinate1, coordinateEnd=coordinate2)
        if None is nodeList:
            return nodeList

        directions = GraphDirection.Direction(nodeList)
        print(nodeList)
        directions.compute_directions()

        return directions.compute_final_directions()


    def __addLinePlot(self, plt, coord1, coord2):
        plt.arrow(coord1[0], coord1[1], coord2[0] - coord1[0], coord2[1] - coord1[1], head_width=0.05, head_length=0.1,
                  fc='k', ec='k')

    def __getNodeFromList(self, nodeList, searchNode):
        for node in nodeList:
            if node.name == searchNode:
                return node
        return None

    def __getSolution(self, node, coordStart):
        solutionCoord = []

        while node.coordinates[0] != coordStart[0] or node.coordinates[1] != coordStart[1]:
            solutionCoord.insert(0, node.coordinates)
            node = node.parent

        solutionCoord.insert(0, coordStart)
        return solutionCoord

    def __UniformCostSearch(self, coordinateStart, coordinateEnd):
        priorityQueueFrontier = PriorityQueue.PriorityQueue()
        listVisit = []
        extractedNode = None

        for node in self.__nodes:
            if node.coordinates == coordinateStart:
                startNode = node
                break


        if startNode == None:
            return None

        searchNode = Node.SearchNode(startNode.neighbors, None, 0, startNode.coordinates)

        priorityQueueFrontier.push(searchNode, searchNode.cost)

        while True:

            if True == priorityQueueFrontier.isEmpty():
                return None

            extractedNode = priorityQueueFrontier.pop();

            # verific daca nodul a mai fost vizitat
            if extractedNode.coordinates in listVisit:
                continue;

            # marcheaza nodul ca fiind vizitat
            listVisit.append(extractedNode.coordinates)

            # varific daca am gasit nodul de final
            if coordinateEnd == extractedNode.coordinates:
                break

            # aici daca se modifica costul pe muchii trebuie modificat
            costMuchie = 1
            for vecin in extractedNode.neighbors:
                coord = vecin.coordinates
                if coord not in listVisit:
                    vecin.parent = extractedNode
                    vecin.cost = extractedNode.cost + costMuchie
                    newVecin = Node.SearchNode(vecin.neighbors, extractedNode, vecin.cost, vecin.coordinates)
                    priorityQueueFrontier.push(newVecin, vecin.cost)

        return self.__getSolution(extractedNode, coordinateStart)



if __name__=="__main__":
    graf = Graph('grafTest.TXT')
    #graf.printGraph()
    #solution = graf.getDirections([1.35, 0.45], [0.9, 2.7])
    solution = graf.getDirections([0.9, 0.0], [0.9, 3.15])
    print(solution)
    graf.showGraph()
#
