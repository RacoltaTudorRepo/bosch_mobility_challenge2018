import math
from PIregulater import PIregulator
from PIDcontroller import PID

msgClass = '--MESSAGE FROM CONTROL STEER ANGLE-- '

goal = 0
Kp = 0.25
Ki = -0.45
Kd = 10

class Line():
    def __init__(self, pointStart, pointEnd):
        global Kp
        global Ki
        global goal

        self.pointStart = pointStart
        self.pointEnd = pointEnd
        self.A = self.__getA()
        self.B = self.__getB()
        self.C = self.__getC()
        self.PI = PIregulator(goal=goal, Kp=Kp, Ki=Ki)
        self.PID = PID(Kp, Ki, Kd)

    def __getA(self):
        return self.pointStart[1] - self.pointEnd[1]

    def __getB(self):
        return self.pointEnd[0] - self.pointStart[0]

    def __getC(self):
        return self.pointStart[0] * self.pointEnd[1] - self.pointEnd[0] * self.pointStart[1]

    def __getPointValue(self, point):
        return self.A * point[0] + self.B * point[1] + self.C

    def getPozPointToLine(self, point):
        value = self.__getPointValue(point)

        if self.pointStart[1] == self.pointEnd[1]:
            # ->
            if self.pointStart[0] < self.pointEnd[0]:
                if point[1] == self.pointStart[1]:
                    return (value, 0)
                elif point[1] > self.pointStart[1]:
                    return (value, 1)
                else:
                    return (value, -1)
            # <-
            elif self.pointStart[0] > self.pointEnd[0]:
                if point[1] == self.pointStart[1]:
                    return (value, 0)
                elif point[1] > self.pointStart[1]:
                    return (value, -1)
                else:
                    return (value, 1)

        elif self.pointStart[0] == self.pointEnd[0]:
            # |>
            if self.pointStart[1] < self.pointEnd[1]:
                if point[0] == self.pointStart[0]:
                    return (value, 0)
                elif point[0] > self.pointStart[0]:
                    return (value, -1)
                else:
                    return (value, 1)
            # <|
            elif self.pointStart[1] > self.pointEnd[1]:
                if point[0] == self.pointStart[0]:
                    return (value, 0)
                elif point[0] > self.pointStart[0]:
                    return (value, 1)
                else:
                    return (value, -1)
        # />
        elif self.pointStart[0] < self.pointEnd[0] and self.pointStart[1] < self.pointEnd[1]:
            if value == 0:
                return (value, 0)
            elif value < 0:
                return (value, -1)
            else:
                return (value, 1)

        # </
        elif self.pointStart[0] > self.pointEnd[0] and self.pointStart[1] > self.pointEnd[1]:
            if value == 0:
                return (value, 0)
            elif value < 0:
                return (value, -1)
            else:
                return (value, 1)

        # \>
        elif self.pointStart[0] < self.pointEnd[0] and self.pointStart[1] > self.pointEnd[1]:
            if value == 0:
                return (value, 0)
            elif value < 0:
                return (value, -1)
            else:
                return (value, 1)

        # <\
        elif self.pointStart[0] > self.pointEnd[0] and self.pointStart[1] < self.pointEnd[1]:
            if value == 0:
                return (value, 0)
            elif value < 0:
                return (value, -1)
            else:
                return (value, 1)

        return (2, 2)


    # deasupra 1, pe ea 0, sub ea -1, se uita la orientarea liniei
    def getPozPointToLineRelativeO(self, point):
        value = self.__getPointValue(point)
        if (value == 0):
            print('Pe linie') # esti pe linie
            return (value, 0)

        if (self.B == 0):
            #linie e perpendiculara pe Ox
            x = -self.C / self.A
            
            # fara orientare
            if point[0] < x: # sub line
                if self.pointStart[1] > self.pointEnd[1]:
                    return (value, -1)
                else:
                    return (value, 1)
            else:
                if self.pointStart[1] > self.pointEnd[1]:
                    return (value, 1)
                else:
                    return (value, -1)
        
        #test deasupra fara orientare
        if (value > 0 and self.B > 0) or (value < 0 and self.B < 0):
            #test cu orientare linie
            if (self.pointStart[0] <= self.pointEnd[0] and self.pointStart[1] <= self.pointEnd[1]):
                return (value, 1)
            else:
                return (value, -1)
        else: #suntem dedesupt
            if (self.pointStart[0] <= self.pointEnd[0] and self.pointStart[1] <= self.pointEnd[1]):
                return (value, -1)
            else:
                return (value, 1)

    # determina daca orientarea masinii este spre linie sau nu
    def gettingCloser2Line(self, point, orientation, valuePoint):
        point2 = (point[0] + orientation[0], point[1] + orientation[1])
        valuePoint2 = self.__getPointValue(point2)
        valuePoint2P = math.fabs(valuePoint2)

        if valuePoint2P > valuePoint:
            return False # ma indepartez
        else:
            return True # ma aproprii

    
    def getAngleWithVector(self, vector):
        lineVector = (self.pointEnd[0] - self.pointStart[0], self.pointEnd[1] - self.pointStart[1])
        lineVectorModule = math.sqrt(lineVector[0] * lineVector[0] + lineVector[1] * lineVector[1])
        lineVectorNorm = (lineVector[0] / lineVectorModule, lineVector[1] / lineVectorModule)

        vectorModule = math.sqrt(vector[0] * vector[0] + vector[1] * vector[1])
        vectorNorm = (vector[0] / vectorModule, vector[1] / vectorModule)
        cosAngle = vectorNorm[0] * lineVectorNorm[0] + vectorNorm[1] * lineVectorNorm[1];
        cosAngle = math.fabs(cosAngle)
        return math.degrees(math.acos(cosAngle))

    def getAngle2(self, valuePos, positionToLine, position, orientation):
        valuePosP = math.fabs(valuePos)
        distanceToLine = valuePosP / math.sqrt(self.A * self.A + self.B * self.B)
        distanceReal = 100 * distanceToLine

        closer = self.gettingCloser2Line(position, orientation, valuePosP)

        angleWithLine = self.getAngleWithVector(orientation)

        printDistances(distanceToLine, distanceReal)
        printGettingClose(closer)
        printAngleWithLine(angleWithLine)

        piValue = self.PI.getValue(distanceToLine) # angleWithLine +
        angle = math.fabs(piValue) - angleWithLine

        #angle = self.PID.update(distanceToLine)

        if positionToLine == -1:
            return -math.fabs(angle)
        else:
            return math.fabs(angle)
        
    def correctAngle(self, angle):
        if angle < -22:
            angle = -22
        elif angle > 22:
            angle = 22
        return angle

    def getSteerAngle(self, point, orientation):
        (valuePoint, relativePosition) = self.getPozPointToLine(point)
        printRelativePosition(relativePosition)
        steerAngle = self.getAngle2(valuePoint, relativePosition, point, orientation)
        steerAngle = self.correctAngle(steerAngle)
        printAngle(steerAngle)

        return steerAngle



def printRelativePosition(relativePosition):
    global msgClass

    if relativePosition == 0:
        print(msgClass, "Masina este pr dreapta")
    elif(relativePosition == 1):
        print(msgClass, "Masina este deasupra liniei")
    elif(relativePosition == -1):
        print(msgClass, "Masina este sub linie")
    else:
        print(msgClass, "Pozitia masinii nu poate fi determinata")

def printAngle(angle):
    global msgClass

    print(msgClass, "Unghiul casculat este:", angle)

def printDistances(distanceToLine, distanceReal):
    global msgClass

    print(msgClass, "Distanta pe graf este:", distanceToLine, "Distanta reala este:", distanceReal)

def printGettingClose(closer):
    global msgClass

    if True == closer:
        print(msgClass, "Masina se apropie de linie")
    else:
        print(msgClass, "Masina se indaparteaza de linie")

def printAngleWithLine(angle):
    global msgClass

    print(msgClass, "Masina face unghi cu linia de:", angle)


def testAngle1():

    print("--TEST 1--")
    source = [1.0, 1]
    orientation = [0.0, 1.0]
    line = Line([0.0, 0.0], [0.0, 4.0])

    angle = line.getSteerAngle(source, orientation)
    print()

def testAngle2():

    print("--TEST 2--");
    source = [1.0, 1]
    orientation = [1.0, 1.0]
    line = Line([0.0, 0.0], [4.0, 0.0])

    angle = line.getSteerAngle(source, orientation)
    print()


def testPosition(testNumber, line, pointUp, pointDown):
    (value, positionUp) = line.getPozPointToLine(pointUp)
    (value, positionDown) = line.getPozPointToLine(pointDown)

    if positionUp != 1:
        print('ERROR POSITION SHOULD BE 1', pointUp, 'TEST NUMBER', testNumber)
    if positionDown != -1:
        print('ERROR POSITION SHOULD BE -1', pointDown, 'TEST NUMBER', testNumber)

# \> ok
def testLine1():
    line = Line([0.45, 0.45], [0.9, 0])
    pointUp = [0.9, 0.45]
    pointDown = [0.6, 0]
    testPosition(1, line, pointUp, pointDown)

# -> ok
def testLine2():
    line = Line([0.9, 0], [1.35, 0])
    pointUp = [1.3, 0.5]
    pointDown = [1.3, -1]
    testPosition(2, line, pointUp, pointDown)

# /> ok
def testLine3():
    line = Line([1.35, 0], [1.8, 0.45])
    pointUp = [1.8, 0.6]
    pointDown = [1.6, 0]
    testPosition(3, line, pointUp, pointDown)

# |> ok
def testLine4():
    line = Line([2.25, 0.9], [2.25, 1.35])
    pointUp = [2, 1.3]
    pointDown = [3, 1.3]
    testPosition(4, line, pointUp, pointDown)

# <\ ok
def testLine5():
    line = Line([2.25, 2.25], [1.8, 2.7])
    pointUp = [2, 2]
    pointDown = [2, 3]
    testPosition(5, line, pointUp, pointDown)

# <- ok
def testLine6():
    line = Line([1.35, 3.05], [0.9, 3.05])
    pointUp = [1.3, 2.5]
    pointDown = [1.3, 4]
    testPosition(6, line, pointUp, pointDown)

# </ ok
def testLine7():
    line = Line([0.9, 3.05], [0.45, 2.7])
    pointUp = [0.7, 2]
    pointDown = [0.7, 3.2]
    testPosition(7, line, pointUp, pointDown)

# <| ok
def testLine8():
    line = Line([0, 2.25], [0, 1.8])
    pointUp = [1, 2]
    pointDown = [-1, 2]
    testPosition(8, line, pointUp, pointDown)

if __name__ == "__main__":
    testAngle1()
    testAngle2()
    testLine1()
    testLine2()
    testLine3()
    testLine4()
    testLine5()
    testLine6()
    testLine7()
    testLine8()
