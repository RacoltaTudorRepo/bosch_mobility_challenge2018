import CarClient
import Utils
from ControlSteerAngle import Line
from GraphDirection import Direction
import math

import SerialHandler
import SerialHandlerTest

from Graph import Graph

#orientarea relativa la sistem (rotim orientarea cu +90)
# da doar liste [x, y]
def getNodes(currentPoint, orientation, destination):
        graf = Graph('CONTEST.txt')
        closestPoint = graf.getClosestPointToGraph(currentPoint, orientation)
        print(closestPoint)
        path = graf.UniformCostSearch(closestPoint, destination)
        if path == None:
                print('Esti prost ma. ai dat start de pe o banda si end pe alta la functia getNodes')
                return None
        Utils.delete90degreeAngles(path)
        directions = Direction(path).compute_directions()

        path.insert(0, currentPoint)
        directions.insert(0, directions[0])

        return path, directions



previousAngle = 0
angleMaxChange = 10 # cat are voie sa se schimbe deodata unghiul


def controlFrana(serialHandler, angle):
    global previousAngle
    global angleMaxChange

    if math.abs(previousAngle - angle) <= angleMaxChange:
        SerialHandlerTest.frana(serialHandler, angle)
    else:
        SerialHandlerTest.frana(serialHandler, 0)

    previousAngle = angle



useDirections = False

def test1():
        global useDirections

        serialHandler=SerialHandler.SerialHandler()
        serialHandler.startReadThread()

        
       

        carInfo = CarClient.CarInfo()
        carInfo.start()
        
        (position, orientation) = carInfo.getInfo()
        print(position)
        destination = [2.25, 2.25]

        SerialHandlerTest.frana(serialHandler, 0)
        serialHandler.close()
        exit()

        points, directions = getNodes(position, orientation, destination)
        if points == None:
                exit()

        print(points)
        print(directions)

        indexStart = 0
        indexEnd = 1
        #orientarea : x + y * j   x- cos y-sin !!!!!!!!!!
        
        while Utils.isCloseOrPass(source=position, orSource=orientation, destination=destination) == False:
            if (indexStart >= points.__len__()):
                print('Ai ratat punctul final')
                break;
            pointStart = points[indexStart]
            pointEnd = points[indexEnd]
            indexStart += 1
            indexEnd += 1
            
            line = Line(pointStart, pointEnd)

            while Utils.isCloseOrPass(source=position, orSource=orientation, destination=pointEnd) == False:
                    angle = line.getSteerAngle(position, orientation)
                    # misca masina dupa unghi

                    # as zice sa testam pe rand ge la inceput sa scoatem asta sa vedem ca macar merge cat de cat
                    if useDirections is True:
                        direction = directions[pointEnd]
                        if direction == 'R':
                            angle = math.abs(angle)  # daca ii dreapta unghi mereu pozitiv
                        elif direction == 'L':
                            angle = -math.abs(angle)  # daca ii stanga unhi mereu negativ


                    controlFrana(serialHandler=serialHandler, angle=angle)
                    SerialHandlerTest.inainteAngle(serialHandler, angle)
                    SerialHandlerTest.frana(serialHandler, 0)


                    (position, orientation) = carInfo.getInfo() # complexe
                    print("Punct in care vreau sa ajung", pointEnd)
                    
        SerialHandlerTest.frana(serialHandler)
        serialHandler.close()

def test2():
        pointComplex = [1.0, 2.0]
        orientation = [1.0, 0.0]
        line = Line([0.0, 0.0], [4.0, 0.0])
        
        angle = line.getSteerAngle(pointComplex, orientation)
        print(angle)

def test3():
        serialHandler=SerialHandler.SerialHandler()
        serialHandler.startReadThread()

        
        

        carInfo = CarClient.CarInfo()
        carInfo.start()

        (position, orientation) = carInfo.getInfo()
        print(position)
        SerialHandlerTest.frana(serialHandler, 0)
        serialHandler.close()
        exit()

import threading
import full_lanedetection_Vfinal


def imageNavigation():
    serialHandler = SerialHandler.SerialHandler()
    serialHandler.startReadThread()

    angleThread = threading.Thread(target=full_lanedetection_Vfinal.pornit_camera())
    angleThread.start()

    while True:

        SerialHandlerTest.inainteAngle(serialHandler, full_lanedetection_Vfinal.get_steerAngle())
        #SerialHandlerTest.frana(serialHandler, 0)

        if full_lanedetection_Vfinal.getFrame() == None:
            break;

    SerialHandlerTest.frana(serialHandler, 0)

    SerialHandlerTest.frana(serialHandler)
    serialHandler.close()

if __name__ == "__main__":
        #imageNavigation()
        test1()
