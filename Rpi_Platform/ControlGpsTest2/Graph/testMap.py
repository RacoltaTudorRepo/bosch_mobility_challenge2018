import json
import matplotlib.pyplot as plt
import Node

def getCoord(listNodes, index):
    for node in listNodes:
        if node[0] == index:
            return node[1]
    return [0, 0]

def addLinePlot(plt, coord1, coord2):
    plt.arrow(coord1[0], coord1[1], coord2[0] - coord1[0], coord2[1] - coord1[1], head_width=0.05, head_length=0.1, fc='k', ec='k')

f = open('HARTA_OVALA_NOUA.TXT', 'r')
mp = json.load(f)

listx = []
listy = []
listNodes = []
listNameCoord = []

for key in mp.keys():
    nodeList = mp[key]
    for el in nodeList:
        for key2 in el.keys():
            if key2 == 'COORDINATES':
                listx.append(el[key2][0])
                listy.append(el[key2][1])
        index = el['NAME']
        inBack = el['IN_BACK']
        inRight = el['IN_RIGHT']
        inAhead = el['IN_AHEAD']
        inLeft = el['IN_LEFT']
        outBack = el['OUT_BACK']
        outRight = el['OUT_RIGHT']
        outAhead = el['OUT_AHEAD']
        outLeft = el['OUT_LEFT']
        coord = el['COORDINATES']

        d = [index, coord]
        listNameCoord.append(d)
        graphNode = Node.NodeContainer(index, inBack, inRight, inAhead, inLeft, outBack, outRight, outAhead, outLeft, coord)
        listNodes.append(graphNode)

#for node in listNodes:
    #print(node.print())

for node in listNodes:
    if node.inBack != 'none':
        addLinePlot(plt, getCoord(listNameCoord, node.inBack), node.coord)
    if node.inLeft != 'none':
        addLinePlot(plt, getCoord(listNameCoord, node.inLeft), node.coord)
    if node.inAhead != 'none':
        addLinePlot(plt, getCoord(listNameCoord, node.inAhead), node.coord)
    if node.inRight != 'none':
        addLinePlot(plt, getCoord(listNameCoord, node.inRight), node.coord)
    if node.outBack != 'none':
        addLinePlot(plt, node.coord, getCoord(listNameCoord, node.outBack))
    if node.outLeft != 'none':
        addLinePlot(plt, node.coord, getCoord(listNameCoord, node.outLeft))
    if node.outRight != 'none':
        addLinePlot(plt, node.coord, getCoord(listNameCoord, node.outRight))
    if node.outAhead != 'none':
        addLinePlot(plt, node.coord, getCoord(listNameCoord, node.outAhead))


plt.plot(listx, listy, 'ro')
plt.show()

listNodesProcesate = []

for key in mp.keys():
    nodeList = mp[key]
    for el in nodeList:
        index = el['NAME']
        listaVecini = []
        listaVecini.append(el['OUT_BACK'])
        listaVecini.append(el['OUT_RIGHT'])
        listaVecini.append(el['OUT_AHEAD'])
        listaVecini.append(el['OUT_LEFT'])
        coord = el['COORDINATES']

        graphNode = Node.NodeProcessed(index, listaVecini, coord)
        listNodesProcesate.append(graphNode)

def getNodeFromList(listaNoduri, nodCautat):
    for node in listaNoduri:
        if node.name == nodCautat:
            return node

for node in listNodesProcesate:
    veciniNoi = []
    for vecin in node.vecini:
        veciniNoi.append(getNodeFromList(listNodesProcesate, vecin))
    node.vecini = veciniNoi
    print(node.print())
