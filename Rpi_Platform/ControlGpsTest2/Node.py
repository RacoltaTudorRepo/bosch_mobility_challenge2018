class StorageNode:
    def __init__(self, name, neighbors, coordinates):
        self.name = name
        self.coordinates = coordinates
        self.neighbors = neighbors

    def string(self):
        coordNeighb = []
        for neighbors in self.neighbors:
            coordNeighb.append(neighbors.coordinates)
        return('Name', self.name, 'Vecini', coordNeighb, 'Coord', self.coordinates)


class SearchNode:
    def __init__(self, neighbors, parent, cost, coordinates):
        self.neighbors = neighbors
        self.parent = parent
        self.cost = cost
        self.coordinates = coordinates

    def string(self):
        coordNeighb = []
        for neighbor in self.neighbors:
            coordNeighb.append(neighbor.coordinates)
        return ('Coordinates', self.coordinates, 'Neighbors', self.neighbors, 'Cost', self.cost, 'Parent', self.parent)
