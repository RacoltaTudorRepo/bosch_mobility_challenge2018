import math

def isCloseEnough(point1, point2):
        epsilon = 0.45 / 0.5

        distance = math.sqrt(
            (point1[0] - point2[0]) * (point1[0] - point2[0]) + (point1[1] - point2[1]) * (point1[1] - point2[1]))
        # print('Distanta la punct', distance)

        print()
        print('-- POSITION CAR --', point1)
        print('-- NEXT POINT --', point2)
        print('-- DISTANCE --', distance)
        print()

        if distance <= epsilon:
            return True
        else:
            return False


def normalizeVector(vector):
        vectorLength = math.sqrt(vector[1] * vector[1] + vector[0] * vector[0])
        vectorNorm = [vector[0] / vectorLength, vector[1] / vectorLength]

        return vectorNorm


limitAngle = 90.0
limitRadians = math.radians(limitAngle)

def isCloseOrPass(source, orSource, destination):
        closeEnough = isCloseEnough(source, destination)
        if True == closeEnough:
            return True

        global limitAngle
        # nu sunt suficient de aproape dar trebuie sa verific daca nu am trecut de punct

        sourseDestinationVector = [destination[0] - source[0], destination[1] - source[1]]
        sDvNorm = normalizeVector(sourseDestinationVector)

        # verifica ca unghiul drintre vectori sa fie mai mic ca 90 sa zicem,
        # deci produs scalar apratine (0, 1]

        scalarProd = sDvNorm[0] * orSource[0] + sDvNorm[1] * orSource[1]
        if (math.cos(limitRadians) < scalarProd and math.cos(0) >= scalarProd):
            return False  # inca am sansa sa ajung
        else:
            return True

def printResult(ok):
        if True == ok:
            print("Am trecut sau sunt destul de aproape")
        else:
            print("Inca pot ajunge la punct")



def test1():
        source = [1, 1]
        orSource = [1, 0]
        destination = [2, 2]

        ok = isCloseOrPass(source, orSource, destination)

        printResult(ok)


def test2():
        source = [1, 1]
        orSource = [1, 0]
        destination = [1, 2]

        ok = isCloseOrPass(source, orSource, destination)

        printResult(ok)

def test3():
        source = [1, 1]
        orSource = [-1, 0]
        destination = [2, 2]

        ok = isCloseOrPass(source, orSource, destination)

        printResult(ok)

def test4():
        source = [1, 1]
        orSource = [0, 1]
        destination = [2, 2]

        ok = isCloseOrPass(source, orSource, destination)

        printResult(ok)

def testCloseOrPass():
        test1()
        test2()
        test3()
        test4()


def delete90degreeAngles(points):
        i = 0
        while i < points.__len__() - 2:
        #for i in range(0, points.__len__() - 2):
                point1 = points[i]
                point2 = points[i + 1]
                point3 = points[i + 2]

                if (point1[0] == point2[0] and point2[1] == point3[1]) or (point1[1] == point2[1] and point2[0] == point3[0]):
                        points.remove(point2)
                i = i + 1

if __name__ == "__main__":
        points = [[3, 6], [3, 4], [5, 4], [5, 2], [5, 0], [6, 0], [7, 0], [8, 0], [8, 2]]
        delete90degreeAngles(points)

        print(points)