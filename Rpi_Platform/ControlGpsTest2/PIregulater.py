msgClass = '--MESSAGE FROM PI REGULATER--'

class PIregulator:
    def __init__(self, goal, Kp, Ki):
        self.goal = goal
        self.Kp = Kp
        self.Ki = Ki
        self.integral = 0
        self.error = 0

    def __getError(self, value):
        self.error = self.goal - value
        printError(self.error)

    def __addError(self):
        self.integral = self.integral + self.error

    def getValue(self, currentValue):
        self.__getError(currentValue)
        self.__addError()

        value = self.Kp * self.error + self.Ki * self.integral
        printValue(value)

        return value


def printError(error):
    print(msgClass, "Eroarea este:", error)

def printValue(value):
    print(msgClass, "Valoara este:", value)