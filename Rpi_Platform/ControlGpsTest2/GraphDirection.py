class Direction() :
    def __init__(self, coordinates):
        self.coordinates = coordinates
        self.directions_1 = []
        self.directions_2 = []

    def compute_directions(self):
        #straightMovement : towards right = 1; towards left = 2; up = 3; down = 4
        straightMovement=0
		
        # Precondition: first move is a straight one
        for i in range(0, self.coordinates.__len__() - 1):
            # movement on y
            if self.coordinates[i][0] == self.coordinates[i+1][0]:
                self.directions_1.append('A')
                if self.coordinates[i][1] < self.coordinates[i+1][1]:
                    straightMovement = 3
                else:
                    straightMovement = 4
					
            # movement on x 
            elif self.coordinates[i][1] == self.coordinates[i+1][1]:
                self.directions_1.append('A')
                if self.coordinates[i][0] < self.coordinates[i+1][0]:
                    straightMovement = 1
                else:
                    straightMovement = 2

            # movement on a diagonal
            else:
                # Previously, the car was moving towards right
                if straightMovement == 1:
                    if self.coordinates[i][1] < self.coordinates[i+1][1]:
                        self.directions_1.append('L')
                    else :
                        self.directions_1.append('R')
                # Previously, the car was moving towards left
                elif straightMovement == 2:
                    if self.coordinates[i][1] < self.coordinates[i+1][1]:
                        self.directions_1.append('R')
                    else:
                        self.directions_1.append('L')
                # Previously, the car was moving up
                elif straightMovement == 3:
                    if self.coordinates[i][0] < self.coordinates[i+1][0]:
                        self.directions_1.append('R')
                    else:
                        self.directions_1.append('L')
                elif straightMovement == 4:
                    if self.coordinates[i][0] < self.coordinates[i+1][0]:
                        self.directions_1.append('L')
                    else:
                        self.directions_1.append('R')
        return self.directions_1

                        
    def compute_final_directions(self):
        i = 0
        while i < self.directions_1.__len__() - 1:
            if self.directions_1[i] == 'L' and self.directions_1[i+1] == 'L':
                self.directions_2.append('LongL')
                i += 1
            elif self.directions_1[i] == 'R' and self.directions_1[i+1] == 'R':
                self.directions_2.append('LongR')
                i += 1
            else:
                self.directions_2.append(self.directions_1[i])
            i += 1
        # Particular case for the last element
        if i == self.directions_1.__len__()-1:
            self.directions_2.append(self.directions_1[i])
        return self.directions_2

            
if __name__ == "__main__":
   # directions = Direction([[1.8, 0.45], [2.25, 0.45], [3.15, 1.35], [3.15, 1.8], [3.15, 2.25], [2.7, 2.7], [2.25, 3.15], [1.8, 3.15], [1.35, 3.15], [0.9, 3.6], [0.45, 4.05], [0.45, 4.5], [0.9, 4.95]])
    directions = Direction([[1.35, 0.45], [0.9, 0.45], [0.45, 0.9], [0.45, 1.35], [0.45, 1.8], [0.9, 2.7]])
    dir1 = directions.compute_directions()
    dir2 = directions.compute_final_directions()
    print(dir1)
    print(dir2)
