import numpy as np
import cv2

cap = cv2.VideoCapture(0)



fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('output1.mp4',fourcc, 20.0, (640,480))


while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Display the resulting frame
    cv2.imshow('frame',frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

    out.write(frame)

    

# When everything done, release the capture
cap.release()
out.release()
cv2.destroyAllWindows()
