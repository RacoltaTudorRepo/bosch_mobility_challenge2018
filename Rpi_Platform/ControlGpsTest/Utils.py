import math

def isCloseEnough(point1, point2):
        epsilon = 0.45 / 0.9

        distance = math.sqrt(
            (point1[0] - point2[0]) * (point1[0] - point2[0]) + (point1[1] - point2[1]) * (point1[1] - point2[1]))
        # print('Distanta la punct', distance)
        if distance <= epsilon:
            return True
        else:
            return False


def normalizeVector(vector):
        vectorLength = math.sqrt(vector[1] * vector[1] + vector[0] * vector[0])
        vectorNorm = [vector[0] / vectorLength, vector[1] / vectorLength]

        return vectorNorm


limitAngle = 90.0
limitRadians = math.radians(limitAngle)

def isCloseOrPass(source, orSource, destination):
        closeEnough = isCloseEnough(source, destination)
        if True == closeEnough:
            return True

        global limitAngle
        # nu sunt suficient de aproape dar trebuie sa verific daca nu am trecut de punct

        sourseDestinationVector = [destination[0] - source[0], destination[1] - source[1]]
        sDvNorm = normalizeVector(sourseDestinationVector)

        # verifica ca unghiul drintre vectori sa fie mai mic ca 90 sa zicem,
        # deci produs scalar apratine (0, 1]

        scalarProd = sourseDestinationVector[0] * orSource[0] + sourseDestinationVector[1] * orSource[1]
        if (math.cos(limitRadians) < scalarProd and math.cos(0) >= scalarProd):
            return False  # inca am sansa sa ajung
        else:
            return True

def printResult(ok):
        if True == ok:
            print("Am trecut sau sunt destul de aproape")
        else:
            print("Inca pot ajunge la punct")



def test1():
        source = [1, 1]
        orSource = [1, 0]
        destination = [2, 2]

        ok = isCloseOrPass(source, orSource, destination)

        printResult(ok)


def test2():
        source = [1, 1]
        orSource = [1, 0]
        destination = [1, 2]

        ok = isCloseOrPass(source, orSource, destination)

        printResult(ok)

def test3():
        source = [1, 1]
        orSource = [-1, 0]
        destination = [2, 2]

        ok = isCloseOrPass(source, orSource, destination)

        printResult(ok)

def test4():
        source = [1, 1]
        orSource = [0, 1]
        destination = [2, 2]

        ok = isCloseOrPass(source, orSource, destination)

        printResult(ok)


if __name__ == "__main__":
        test1()
        test2()
        test3()
        test4()
