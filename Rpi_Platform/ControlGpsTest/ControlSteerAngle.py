import math
from PIregulater import PIregulator

msgClass = '--MESSAGE FROM CONTROL STEER ANGLE-- '

goal = 0
Kp = 0.25
Ki = -0.45

class Line():
    def __init__(self, pointStart, pointEnd):
        global Kp
        global Ki
        global goal

        self.pointStart = pointStart
        self.pointEnd = pointEnd
        self.A = self.__getA()
        self.B = self.__getB()
        self.C = self.__getC()
        self.PI = PIregulator(goal=goal, Kp=Kp, Ki=Ki)

    def __getA(self):
        return self.pointStart[1] - self.pointEnd[1]

    def __getB(self):
        return self.pointEnd[0] - self.pointStart[0]

    def __getC(self):
        return self.pointStart[0] * self.pointEnd[1] - self.pointEnd[0] * self.pointStart[1]

    def __getPointValue(self, point):
        return self.A * point[0] + self.B * point[1] + self.C

    # deasupra 1, pe ea 0, sub ea -1, se uita la orientarea liniei
    def getPozPointToLine(self, point):
        value = self.__getPointValue(point)
        if (value == 0):
            print('Pe linie'); # esti pe linie
            return (value, 0)

        if (self.B == 0):
            #linie e perpendiculara pe Ox
            x = -self.C / self.A
            
            # fara orientare
            if point[0] < x: # sub line
                if self.pointStart[1] > self.pointEnd[1]:
                    return (value, -1)
                else:
                    return (value, 1)
            else:
                if self.pointStart[1] > self.pointEnd[1]:
                    return (value, 1)
                else:
                    return (value, -1)
        
        #test deasupra fara orientare
        if (value > 0 and self.B > 0) or (value < 0 and self.B < 0):
            #test cu orientare linie
            if (self.pointStart[0] <= self.pointEnd[0] and self.pointStart[1] <= self.pointEnd[1]):
                return (value, -1)
            else:
                return (value, 1)
        else: #suntem dedesupt
            if (self.pointStart[0] <= self.pointEnd[0] and self.pointStart[1] <= self.pointEnd[1]):
                return (value, 1)
            else:
                return (value, -1)


    def getAngle(self, value, position, orientation):

        valuePoz = math.fabs(value)
        distance = valuePoz / math.sqrt(self.A * self.A + self.B * self.B)
        distanceRead = 45 * distance / 0.45
        
        
        if (position == 0): # pe linie
            return 0
        if (position == 1): # deasupra
            return distanceRead/10
        if (position == -1): # dedesupt
            return -distanceRead/10

    # determina daca orientarea masinii este spre linie sau nu
    def gettingCloser2Line(self, point, orientation, valuePoint):
        point2 = (point[0] + orientation[0], point[1] + orientation[1])
        valuePoint2 = self.__getPointValue(point2)
        valuePoint2P = math.fabs(valuePoint2)

        if valuePoint2P > valuePoint:
            return False # ma indepartez
        else:
            return True # ma aproprii

    
    def getAngleWithVector(self, vector):
        lineVector = (self.pointEnd[0] - self.pointStart[0], self.pointEnd[1] - self.pointStart[1])
        lineVectorModule = math.sqrt(lineVector[0] * lineVector[0] + lineVector[1] * lineVector[1])
        lineVectorNorm = (lineVector[0] / lineVectorModule, lineVector[1] / lineVectorModule)

        vectorModule = math.sqrt(vector[0] * vector[0] + vector[1] * vector[1])
        vectorNorm = (vector[0] / vectorModule, vector[1] / vectorModule)
        cosAngle = vectorNorm[0] * lineVectorNorm[0] + vectorNorm[1] * lineVectorNorm[1];
        cosAngle = math.fabs(cosAngle)
        return math.degrees(math.acos(cosAngle))

    def getAngle2(self, valuePosP, positionToLine, position, orientation):
        distanceToLine = valuePosP / math.sqrt(self.A * self.A + self.B * self.B)
        distanceReal = 100 * distanceToLine

        closer = self.gettingCloser2Line(position, orientation, valuePosP)

        angleWithLine = self.getAngleWithVector(orientation)

        printDistances(distanceToLine, distanceReal)
        printGettingClose(closer)
        printAngleWithLine(angleWithLine)

        piValue = self.PI.getValue(distanceToLine) # angleWithLine +
        angle = piValue - angleWithLine

        if positionToLine == -1:
            return -math.fabs(angle)
        else:
            return math.fabs(angle)
        '''if closer == False: # ma indepartez virez maxim

            if positionToLine == 1: # ma departez si sunt deasupra
                return 22.0
            else: # ma departez si sunt sub linie
                return -22.0 
        else:

            steer = angleWithLine + distanceToLine
            if steer < 0.0:
                steer = 0.0
            if positionToLine == 1: # ma apropii si sunt deasupra
                return steer 
            else: # ma apropii si sunt dedesupt
                return -steer  '''
        
    def correctAngle(self, angle):
        if angle < -22:
            angle = -22
        elif angle > 22:
            angle = 22
        return angle

    def getSteerAngle(self, point, orientation):
        (valuePoint, relativePosition) = self.getPozPointToLine(point)
        printRelativePosition(relativePosition)
        #steerAngle = self.getAngle(valuePoint, relativePosition, orientation)
        steerAngle = self.getAngle2(math.fabs(valuePoint), relativePosition, point, orientation)
        steerAngle = self.correctAngle(steerAngle)
        printAngle(steerAngle)

        return steerAngle



def printRelativePosition(relativePosition):
    global msgClass

    if relativePosition == 0:
        print(msgClass, "Masina este pr dreapta")
    elif(relativePosition == 1):
        print(msgClass, "Masina este deasupra liniei")
    elif(relativePosition == -1):
        print(msgClass, "Masina este sub linie")
    else:
        print(msgClass, "Pozitia masinii nu poate fi determinata")

def printAngle(angle):
    global msgClass

    print(msgClass, "Unghiul casculat este:", angle)

def printDistances(distanceToLine, distanceReal):
    global msgClass

    print(msgClass, "Distanta pe graf este:", distanceToLine, "Distanta reala este:", distanceReal)

def printGettingClose(closer):
    global msgClass

    if True == closer:
        print(msgClass, "Masina se apropie de linie")
    else:
        print(msgClass, "Masina se indaparteaza de linie")

def printAngleWithLine(angle):
    global msgClass

    print(msgClass, "Masina face unghi cu linia de:", angle)


def testAngle1():

    print("--TEST 1--")
    source = [1.0, 1]
    orientation = [1.0, 0.0]
    line = Line([0.0, 0.0], [0.0, 4.0])

    angle = line.getSteerAngle(source, orientation)
    print()

def testAngle2():

    print("--TEST 2--");
    source = [1.0, 1]
    orientation = [1.0, 1.0]
    line = Line([0.0, 0.0], [4.0, 0.0])

    angle = line.getSteerAngle(source, orientation)
    print()

if __name__ == "__main__":
    testAngle1()
    testAngle2()
