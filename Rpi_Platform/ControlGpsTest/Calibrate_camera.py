import glob
import numpy as np
import cv2
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import json

cal_image_loc = glob.glob('camera_cal/*.jpg')
calibration_images = []
global mtx
global dist

def get_mtx():
    print("mtx gasit este:", mtx)
    return mtx


def get_dist():
    print("dist gasit este:", dist)
    return dist


def calibrare():
    for fname in cal_image_loc:
        img = mpimg.imread(fname)
        calibration_images.append(img)

    # in object points sunt coordonatele colturilor reale ale tablei de sah
    objp = np.zeros((6 * 9, 3), np.float32)
    objp[:, :2] = np.mgrid[0:9, 0:6].T.reshape(-1, 2)

    # punctele tablei de sah se vor salva aici
    objpoints = []
    imgpoints = []  # in img points se salveaza colturile tablei de sah imaginare


    # gasim punctele
    for image in calibration_images:
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        ret, corners = cv2.findChessboardCorners(gray, (9, 6), None)  # 9 si 6 sunt pattern sizes
        if ret is True:
            objpoints.append(objp)
            imgpoints.append(corners)
            cv2.drawChessboardCorners(image, (9, 6), corners, ret)

    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)
    file = open("valori.txt", "w")
    data={}
    data["mtx"]=mtx.tolist()
    data["dist"]=dist.tolist()
    dataJson = json.dumps(data)
    #print(dataJson)
    file.write(str(dataJson))
    # file.write(str(dist))
    file.close()
    print("mtx =", mtx)
    print("dist = ", dist)

if __name__ == "__main__":
         calibrare()
