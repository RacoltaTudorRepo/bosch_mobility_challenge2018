import numpy as np
#import os
import cv2
from imutils.video import FPS
from imutils.video.pivideostream import PiVideoStream
from picamera.array import PiRGBArray
from picamera import PiCamera 
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import imutils
import glob
import time


# 1280 x 720

cal_image_loc = glob.glob('camera_cal/*.jpg')
calibration_images = []


for fname in cal_image_loc:
    img = mpimg.imread(fname)
    calibration_images.append(img)

# in object points sunt coordonatele colturilor reale ale tablei de sah
objp = np.zeros((6 * 9, 3), np.float32)
objp[:, :2] = np.mgrid[0:9, 0:6].T.reshape(-1, 2)

# punctele tablei de sah se vor salva aici
objpoints = []
imgpoints = []  # in img points se salveaza colturile tablei de sah imaginare


# gasim punctele
for image in calibration_images:
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    ret, corners = cv2.findChessboardCorners(gray, (9, 6), None)  # 9 si 6 sunt pattern sizes
    if ret is True:
        objpoints.append(objp)
        imgpoints.append(corners)
        cv2.drawChessboardCorners(image, (9, 6), corners, ret)

ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)


# clasa caracteristici
class Line:
    def __init__(self):
        # aici daca am avut linie in iteratia anterioara
        self.detected = False
        # coefficientii recenti ai polinomului care i-am facut fit
        self.recent_fit = []
        # coeficientii polinomului fituit, la care e facuta o medie artim peste ultimele n iteratii
        self.best_fit = None
        # coeficientii polin pt fitul cel mai recent, folosit aqm
        self.current_fit = [np.array([False])]
        # diferenta in fit, la coeficientii trecuti si cei de acuma
        self.diffs = np.array([0, 0, 0], dtype='float')
        # valorile in x pentru pixelii de linie detectati
        self.allx = None
        # valorile in y
        self.ally = None
        # counterul asta il folosim doar ca sa resetam daca nu gasim linie. aici e ales la 5 sa reseteze,
        self.counter = 0


def pipeline(imgpipe, s_thresh=(10, 100), sx_thresh=(10, 100), R_thresh=(140, 230), sobel_kernel=3):
    
    # RED 140 224
    distorted_img = np.copy(imgpipe)
    # cv2.imwrite('verif_imagine_binara_dst.jpg', distorted_img)
    # cv2.imshow('verif_img_dist',distorted_img)
    dst = cv2.undistort(distorted_img, mtx, dist, None, mtx)
    # cv2.imwrite('verif_img_undst.jpg', dst)
    # cv2.imshow('verif_img_undst',dst)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    R = dst[:, :, 0]
  
    hls = cv2.cvtColor(dst, cv2.COLOR_RGB2HLS).astype(np.float)
    # cv2.imshow('verif_img_hls',hls)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    h_channel = hls[:, :, 0]
    l_channel = hls[:, :, 1]
    s_channel = hls[:, :, 2]

    # image gradient Sobelx - takes the derivate in x, absolute value, then rescale
    # https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_imgproc/py_gradients/py_gradients.html
    # sobel e mai rezistent la noise. e un gaussian cu ceva diferentieri care il ajuta
    # x sau y e directia derivatei 1 0 vine de la faptul ca vrem pe x 
    sobelx = cv2.Sobel(l_channel, cv2.CV_64F, 1, 0, ksize=sobel_kernel)
    abs_sobelx = np.absolute(sobelx)
    scaled_sobelx = np.uint8(255 * abs_sobelx / np.max(abs_sobelx))

    sxbinary = np.zeros_like(scaled_sobelx)  # Return an array of zeros with the same shape and type as a given array.
    sxbinary[(scaled_sobelx >= sx_thresh[0]) & (scaled_sobelx <= sx_thresh[1])] = 1
    #sxbinarye=sxbinary*255
    # cv2.imshow('verif_img_sxbinary', sxbinarye)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    r_binary = np.zeros_like(R)

    r_binary[(R >= R_thresh[0]) & (R <= R_thresh[1])] = 1
    #r_binarye=r_binary*255
    #cv2.imshow('verif_img_rbinary',r_binarye)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
    # threshold color channel
    # folosim doar S din HLS color channel = hue saturation lightness. mai exista HSV
    s_binary = np.zeros_like(s_channel)
    s_binary[(s_channel >= s_thresh[0]) & (s_channel <= s_thresh[1])] = 1
    #s_binarye=s_binary*255

    #l_binary = np.zeros_like(l_channel)
    #l_binary[(l_channel >= l_thresh[0]) & (l_channel <= l_thresh[1])] = 1
    #l_binarye = l_binary * 255
    #cv2.imshow('verif_img_slinary',l_binarye)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()

    combined_binary = np.zeros_like(sxbinary)
    combined_binary[((s_binary == 1) & (sxbinary == 1)) |
                    ((sxbinary == 1) & (r_binary == 1)) |
                    ((s_binary == 1) & (r_binary == 1))] = 1
    #totalbinerye = combined_binary*255
    #cv2.imshow('verificare img binara in functie',totalbinerye)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
    return combined_binary


def birds_eye(img, mtx, dist):
    binary_img = pipeline(img)
    #cv2.imwrite('verif_imagine_binara.jpg',binary_img)
    #cv2.imshow('verif_img_binara',binary_img)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
    undist = cv2.undistort(binary_img, mtx, dist, None, mtx)
    # cv2.imwrite('verif_imagine_binara_undist.jpg', binary_img)
    #cv2.imshow('verif_img_binara_undst',undist)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    img_size = (undist.shape[1], undist.shape[0])

    # print(img_size)
    # punctele sursa: de unde vrem sa ia imaginea. punem colturile de jos a imaginii - defined area of lane line edges
    src = np.float32([[img_size[0], img_size[1]/2+14], [img_size[0], img_size[1]-5], [0, img_size[1]-5], [3, img_size[1]/2+14]])
    # and the top of the lines, putin mai sus de orizont

    # 4 destination points to transfer
    offset = 20  # offset for dst points
    dst = np.float32([[img_size[0] - offset, 0], [img_size[0] - offset, img_size[1]],
                      [offset, img_size[1]], [offset, 0]])

    M = cv2.getPerspectiveTransform(src, dst)

    # asta e necesara pt birds EYE. atentia la IMG SIZE, paote se strica de aici. M e transform matrix.
    # trebuie sa dea bine ca sa faca bine
    top_down = cv2.warpPerspective(undist, M, img_size)
    top_downe = top_down*255
    #cv2.imshow('verif_top_down_birdseye.jpg', top_downe)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()

    return top_down, M


def count_check(line):
    # folosit ca sa resetam cautarea, daca cumva nu gaseste o linie dupa 5 incercari de a gasi
    if line.counter >= 5:
        line.detected = False


def first_lines(img, mtx, dist):
    # liniile astea prime sunt folosite pentru a creea o histograma de unde incep frameurile binare activate si
    # foloseste slidiing windows de-alungul marginilor pentru a estima unde

    binary_warped, perspective_M = birds_eye(img, mtx, dist)
    # cv2.imshow('verif_img_binara_undst', binary_warped)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    histogram = np.sum(binary_warped[int(binary_warped.shape[0] / 2):, :], axis=0)

    # Output image an to draw on and visualize the result
    out_img = np.dstack((binary_warped, binary_warped, binary_warped)) * 255

    # cauta peakurile partii din stanga si dreapta a histogramei. acestea sunt punctele de plecare pentru liniile
    # din stanga si dreapta ale laneului
    midpoint = np.int(histogram.shape[0] / 2)
    leftx_base = np.argmax(histogram[:midpoint])
    rightx_base = np.argmax(histogram[midpoint:]) + midpoint

    # cate windowuri avem
    nwindows = 9

    # inaltimea unui window
    window_height = np.int(binary_warped.shape[0] / nwindows)

    # identify the x and y positions of all nonzero pixels in the image. nonzero inseamna ca , cum e binara imaginea,
    #  e din zona care ne intereseaza daca e 1.
    nonzero = binary_warped.nonzero()
    nonzeroy = np.array(nonzero[0])
    nonzerox = np.array(nonzero[1])

    # pozitiile curente care le updatam pt fiecare window
    leftx_current = leftx_base
    rightx_current = rightx_base

    # fiecare window are o toleranta sa zicem asa. un margin
    margin = 100

    #  nr minim de pixeli gasiti pt a recentra windowul
    minpix = 50

    #  listele goale care primesc left and right lane pixel indices, indicii sunt importanti si trebuie sa ii verific.
    # daca is probleme poate e de la indici sau
    # de la windowuri
    left_lane_inds = []
    right_lane_inds = []

    # parcurgem windowurile una cate una pentru a vedea cum construim laneuurile
    for window in range(nwindows):
        # window boundaries in x and y (and right and left)
        # pe aici ai mereu cu non zero pt ca e imagine binara, adica 0 sau 1. si peste tot  pe unde ai 1 ti le ia
        # in considerare
        win_y_low = binary_warped.shape[0] - (window + 1) * window_height
        win_y_high = binary_warped.shape[0] - window * window_height
        win_xleft_low = leftx_current - margin
        win_xleft_high = leftx_current + margin
        win_xright_low = rightx_current - margin
        win_xright_high = rightx_current + margin
        # pune windowurile pe imagine. daca ii dam print la imagine vedem unde le pune si unde cauta liniile
        cv2.rectangle(out_img, (win_xleft_low, win_y_low), (win_xleft_high, win_y_high), (0, 255, 0), 2)
        cv2.rectangle(out_img, (win_xright_low, win_y_low), (win_xright_high, win_y_high), (0, 255, 0), 2)
        # cauta nonzero pixels in x si y si salvam indicii pixelilor
        good_left_inds = ((nonzeroy >= win_y_low) & (nonzeroy < win_y_high) & (nonzerox >= win_xleft_low) & (
                    nonzerox < win_xleft_high)).nonzero()[0]
        good_right_inds = ((nonzeroy >= win_y_low) & (nonzeroy < win_y_high) & (nonzerox >= win_xright_low) & (
                    nonzerox < win_xright_high)).nonzero()[0]
        # adaugam indicile astea bune la lista goala de mai sus
        left_lane_inds.append(good_left_inds)
        right_lane_inds.append(good_right_inds)
        # daca cumva se gasesc mai multi indici decat e minimul, se recentreaza imaginea urmatoare on their mean
        # position
        if len(good_left_inds) > minpix:
            leftx_current = np.int(np.mean(nonzerox[good_left_inds]))
        if len(good_right_inds) > minpix:
            rightx_current = np.int(np.mean(nonzerox[good_right_inds]))

        print(left_lane_inds, right_lane_inds, good_left_inds, good_right_inds)

    left_lane_inds = np.concatenate(left_lane_inds)
    right_lane_inds = np.concatenate(right_lane_inds)

    # extragem left and right line pixel positions. avem nevoie de ele mai jos
    leftx = nonzerox[left_lane_inds]
    lefty = nonzeroy[left_lane_inds]
    rightx = nonzerox[right_lane_inds]
    righty = nonzeroy[right_lane_inds]

    # fit a second order polynomial to each left and right. cateodata videourile dau errori de aceea folosim try catch.
    # . dupa o eroare, punem line.detected False
    # pentru ca sa reluam procesul. iara stanga apoi dreapta.

    try:
        n = 5
        left_line.current_fit = np.polyfit(lefty, leftx, 2)
        left_line.all_x = leftx
        left_line.all_y = lefty
        left_line.recent_fit.append(left_line.current_fit)
        if len(left_line.recent_fit) > 1:
            left_line.diffs = (left_line.recent_fit[-2] - left_line.recent_fit[-1]) / left_line.recent_fit[-2]
        left_line.recent_fit = left_line.recent_fit[-n:]
        left_line.best_fit = np.mean(left_line.recent_fit, axis=0)
        left_fit = left_line.current_fit
        left_line.detected = True
        left_line.counter = 0
    except TypeError:
        left_fit = left_line.best_fit
        left_line.detected = False
    except np.linalg.LinAlgError:
        left_fit = left_line.best_fit
        left_line.detected = False

    #print("stanga detectat", left_line.detected)

    try:
        n = 5
        right_line.current_fit = np.polyfit(righty, rightx, 2)
        right_line.all_x = rightx
        right_line.all_y = righty
        right_line.recent_fit.append(right_line.current_fit)
        if len(right_line.recent_fit) > 1:
            right_line.diffs = (right_line.recent_fit[-2] - right_line.recent_fit[-1]) / right_line.recent_fit[-2]
        right_line.recent_fit = right_line.recent_fit[-n:]
        right_line.best_fit = np.mean(right_line.recent_fit, axis=0)
        right_fit = right_line.current_fit
        right_line.detected = True
        right_line.counter = 0
    except TypeError:
        right_fit = right_line.best_fit
        right_line.detected = False
    except np.linalg.LinAlgError:
        right_fit = right_line.best_fit
        right_line.detected = False

    #print("dreapta detectat", right_line.detected)


def second_ord_poly(line, val):
    # aceasta functie de second order e folosita pentru ca sa calculezi distanta de la centru imaginii la centrul lane.
    #  Cauta baza liniei de la bazajosul imaginii
    a = line[0]
    b = line[1]
    c = line[2]
    formula = (a * val ** 2) + (b * val) + c

    return formula


def draw_lines(img, mtx, dist):
    # functia asta verifica in ea daca detectam linie. Daca nu, ne trimite inapoi la first lines. Daca detecteaza,
    #  cautam numa in windowuri dupa linii.
    # apoi desenam liniile si vedem unde e masina in relatie cu centrul laneului

    global dist_from_center

    binary_warped, perspective_M = birds_eye(img, mtx, dist)

    # cv2.imshow('verificare in functia draw lines, img binara',binary_warped)

    if left_line.detected is False | right_line.detected is False:
        first_lines(img, mtx, dist)

    # the current fit for now (set now modify later)
    left_fit = left_line.current_fit
    right_fit = right_line.current_fit

    #print(left_fit, right_fit)

    # the lane indicators
    nonzero = binary_warped.nonzero()
    nonzeroy = np.array(nonzero[0])
    nonzerox = np.array(nonzero[1])
    margin = 100
    left_lane_inds = ((nonzerox > (left_fit[0] * (nonzeroy ** 2) + left_fit[1] * nonzeroy + left_fit[2] - margin)) &
                      (nonzerox < (left_fit[0] * (nonzeroy ** 2) + left_fit[1] * nonzeroy + left_fit[2] + margin)))
    right_lane_inds = ((nonzerox > (right_fit[0] * (nonzeroy ** 2) + right_fit[1] * nonzeroy + right_fit[2] - margin)) &
                       (nonzerox < (right_fit[0] * (nonzeroy ** 2) + right_fit[1] * nonzeroy + right_fit[2] + margin)))

    # aceeasi poveste cu non zero x si y, unde e 1 in imaginea binara inseamna ca e regiune ce ne intereseaza deci e
    # nonzero pe x si y
    leftx = nonzerox[left_lane_inds]
    lefty = nonzeroy[left_lane_inds]
    rightx = nonzerox[right_lane_inds]
    righty = nonzeroy[right_lane_inds]

    # fit a second order polynomial to each . iara facem try catch ca poate da erori si incepem cu linia stanga prima
    # oara, apoi cu dreapta
    try:
        n = 5
        left_line.current_fit = np.polyfit(lefty, leftx, 2)
        left_line.all_x = leftx
        left_line.all_y = lefty
        left_line.recent_fit.append(left_line.current_fit)
        if len(left_line.recent_fit) > 1:
            left_line.diffs = (left_line.recent_fit[-2] - left_line.recent_fit[-1]) / left_line.recent_fit[-2]
        left_line.recent_fit = left_line.recent_fit[-n:]
        left_line.best_fit = np.mean(left_line.recent_fit, axis=0)
        left_fit = left_line.current_fit
        left_line.detected = True
        left_line.counter = 0
    except TypeError:
        left_fit = left_line.best_fit
        count_check(left_line)
    except np.linalg.LinAlgError:
        left_fit = left_line.best_fit
        count_check(left_line)

        #print("Vezi left fit la trycatch de gasire"+left_fit)

    try:
        n = 5
        right_line.current_fit = np.polyfit(righty, rightx, 2)
        right_line.all_x = rightx
        right_line.all_y = righty
        right_line.recent_fit.append(right_line.current_fit)
        if len(right_line.recent_fit) > 1:
            right_line.diffs = (right_line.recent_fit[-2] - right_line.recent_fit[-1]) / right_line.recent_fit[-2]
        right_line.recent_fit = right_line.recent_fit[-n:]
        right_line.best_fit = np.mean(right_line.recent_fit, axis=0)
        right_fit = right_line.current_fit
        right_line.detected = True
        right_line.counter = 0
    except TypeError:
        right_fit = right_line.best_fit
        count_check(right_line)
    except np.linalg.LinAlgError:
        right_fit = right_line.best_fit
        count_check(right_line)

        #print("vezi right fit la tryctach de gasire"+right_fit)

    # generate x and y values for plotting
    fity = np.linspace(0, binary_warped.shape[0] - 1, binary_warped.shape[0])
    fit_leftx = left_fit[0] * fity ** 2 + left_fit[1] * fity + left_fit[2]
    fit_rightx = right_fit[0] * fity ** 2 + right_fit[1] * fity + right_fit[2]

    # putem output o imagine pe care aaratam selection windows si unde pe care o sa desenam in continuare
    out_img = np.dstack((binary_warped, binary_warped, binary_warped)) * 255
    window_img = np.zeros_like(out_img)
    # cv2.imshow('verificam windowjpg',window_img)

    # punem niste culori peste liniile laneului din stanga si dreapta
    out_img[nonzeroy[left_lane_inds], nonzerox[left_lane_inds]] = [255, 0, 0]
    out_img[nonzeroy[right_lane_inds], nonzerox[right_lane_inds]] = [0, 0, 255]

    # generam un polygon, cu care sa aratam search windowul si recast the x and y points ca sa poate fi folosit de
    # functia cv2.fillPoly()
    left_line_window1 = np.array([np.transpose(np.vstack([fit_leftx - margin, fity]))])
    left_line_window2 = np.array([np.flipud(np.transpose(np.vstack([fit_leftx + margin, fity])))])
    left_line_pts = np.hstack((left_line_window1, left_line_window2))
    right_line_window1 = np.array([np.transpose(np.vstack([fit_rightx - margin, fity]))])
    right_line_window2 = np.array([np.flipud(np.transpose(np.vstack([fit_rightx + margin, fity])))])
    right_line_pts = np.hstack((right_line_window1, right_line_window2))

    # calculez pixel curve radius
    # y_eval = np.max(fity)
    # left_curverad = ((1 + (2 * left_fit[0] * y_eval + left_fit[1]) ** 2) ** 1.5) / np.absolute(2 * left_fit[0])
    # right_curverad = ((1 + (2 * right_fit[0] * y_eval + right_fit[1]) ** 2) ** 1.5) / np.absolute(2 * right_fit[0])

    # convertim x si y din pixels space to4 meters | de pe udemy le-am luat de la un tip si apoi m-am jucat cu ele
    ym_per_pix = 3 / 320  # meters per pixel in y dimension
    xm_per_pix = 0.5 / 480  # meters per pixel in x dimension

    # polinoame noi pentru x si y pentru noua convertires
    left_fit_cr = np.polyfit(left_line.all_y * ym_per_pix, left_line.all_x * xm_per_pix, 2)
    right_fit_cr = np.polyfit(right_line.all_y * ym_per_pix, right_line.all_x * xm_per_pix, 2)

    # calculam mijlocul imaginii, judecand ca e si mijlocul masinii. camera sa fie in mijlocul masinii
    middle_of_image = img.shape[1] / 2
    #print("Middle of img"+str(middle_of_image))
    car_position = middle_of_image * xm_per_pix

    # calculam mijlocul laneului pentru a putea face corectii
    left_line_base = second_ord_poly(left_fit_cr, img.shape[0] * ym_per_pix)
    right_line_base = second_ord_poly(right_fit_cr, img.shape[0] * ym_per_pix)
    lane_mid = (left_line_base + right_line_base) / 2
    print("lane mid="+str(lane_mid))

    # CENTRU LINIE
    dist_from_center = lane_mid - car_position
    print("")
    print(dist_from_center)
    #if dist_from_center >= 0:
        #center_text = "{} meters left of center".format(round(dist_from_center, 2))
    #else:
        #center_text = "{} meters right of center".format(round(-dist_from_center, 2))

    # aici doar scriem pozitia masinii fata de centru
    # font = cv2.FONT_HERSHEY_SIMPLEX
    # cv2.putText(img, center_text, (10, 50), font, 1, (255, 255, 255), 2)

    # aducem inapoi transform matrixul din birdseye ( refacem si imaginea inapoi cum e normala nu bird eye)
    minv = np.linalg.inv(perspective_M)

    # facem o imagine pe care sa desenam liniile laneului
    warp_zero = np.zeros_like(binary_warped).astype(np.uint8)
    color_warp = np.dstack((warp_zero, warp_zero, warp_zero))

    # facem un recast la x si y pentru ca sa poata sa o foloseasca cv2.fillPoly(). nu am inteles 100% de ce
    pts_left = np.array([np.transpose(np.vstack([fit_leftx, fity]))])
    pts_right = np.array([np.flipud(np.transpose(np.vstack([fit_rightx, fity])))])
    pts = np.hstack((pts_left, pts_right))

    # desenam pe lane . de aici vine culoarea alba ca asa i-am dat eu
    # cv2.fillPoly(color_warp, np.int_([pts]), (255, 255, 255))

    # aici facem un warp inapoi in original image space folosing inverse perspective matrix (Minv)
    newwarp = cv2.warpPerspective(color_warp, minv, (img.shape[1], img.shape[0]))

    # combinam rezultatul cu imaginea originala pentru ca sa vedem cum e normal imaginea
    result = cv2.addWeighted(img, 1, newwarp, 0.3, 0)

    return result

def get_center():
    return dist_from_center


def process_image(image):
    #print("shape img:", image.shape
    result = draw_lines(image, mtx, dist)
    return result


# set the class lines
left_line = Line()
right_line = Line()


def pornit_camera():
    vs = PiVideoStream().start()
    time.sleep(2.0)
    # fps = FPS().start()

    while True:
        image = vs.read()
        image = imutils.resize(image, width=480, height=320)
        process_image(image)
        key = cv2.waitKey(10) & 0xff
        #fps.update()
        if key == 27:
            break

    # fps.stop()
    # print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))
    cv2.destroyAllWindows()
    vs.stop()   
    

if __name__ == "__main__":
         pornit_camera()
    
