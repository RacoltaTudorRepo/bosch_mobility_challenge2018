carHeight = 0.17
carWidth = 0.1 / 2

import math

def testCarPosAlg(carPos, carOr):

    carOr90Right = [carOr[1], -carOr[0]]

    heightVectorScale = [carOr[0] * carHeight, carOr[1] * carHeight]
    widthVectorScale = [carOr90Right[0] * carWidth, carOr90Right[1] * carWidth]

    print(heightVectorScale)
    print(widthVectorScale)

    carCenterPos = [carPos[0] + heightVectorScale[0] + widthVectorScale[0],
                    carPos[1] + heightVectorScale[1] + widthVectorScale[1]]

    # print(car_poz1, car_or1)
    return (carCenterPos, carOr)

if __name__ == "__main__":
    #testCarInfo()
    (carPos, carOr) = testCarPosAlg([1, 1], [1 / math.sqrt(2), 1/ math.sqrt(2)])
    print(carPos)