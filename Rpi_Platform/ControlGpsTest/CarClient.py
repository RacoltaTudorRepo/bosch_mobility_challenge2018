from threading import Thread, Lock, Event
from socket import *
from math import *
from random import *
from time import *
import os
import sys

THIS_CAR_ID = 1
gw = os.popen("ip -4 route show default").read().split()  # creare type
s = socket(AF_INET, SOCK_DGRAM)
s.connect((gw[2], 0))
HOST_NAME = gethostname()
HOST_IP = s.getsockname()[0]
s.close()
print("Host name:" + str(HOST_NAME) + " IP:" + str(HOST_IP))

NEGOTIATION_PORT = 12346
CAR_SUBSCRITPION_PORT = NEGOTIATION_PORT + 2
CAR_COMMUNICATION_PORT = CAR_SUBSCRITPION_PORT + 2
MAX_WAIT_TIME_FOR_SERVER = 10
SERVER_IP = None
car_id = THIS_CAR_ID
car_poz = 0 + 0j
car_or = 0 + 0j
NEW_SERVER_IP = False
START_UP = True
G_Socket_Poz = socket()
ID_SENT_FLAG = False
RUN_CARCLIENT = True


carHeight = 0.70
carWidth = 0.1 / 2

changeEvent = Event()

# ------------------------------------------------------------------------
def GetServer():
    # listen for server broadcast and extract server IP address
    global SERVER_IP
    global NEGOTIATION_PORT
    global NEW_SERVER_IP
    global G_Socket_Poz
    global START_UP
    global ID_SENT_FLAG
    global RUN_CARCLIENT

    while RUN_CARCLIENT:
        try:
            s = socket(AF_INET, SOCK_DGRAM)
            s.bind(('', NEGOTIATION_PORT))
            #s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
            s.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)

            WAIT_TIME = 2 + floor(10 * MAX_WAIT_TIME_FOR_SERVER * random()) / 10
            # print("Waiting " + str(WAIT_TIME) + " seconds for server" )
            t = time()
            server = []

            # listen for server broadcast
            s.settimeout(WAIT_TIME)
            data, server_ip = s.recvfrom(1500, 0)
            if server_ip[0] != SERVER_IP:
                # new server
                SendIDToServer(server_ip[0])
                NEW_SERVER_IP = True
                SERVER_IP = server_ip[0]  # server is alive
                START_UP = False
            else:
                # old server
                if ID_SENT_FLAG == False:
                    SendIDToServer(server_ip[0])
                    print("Subscribe @ GPS server")
                NEW_SERVER_IP = False
            # server beacon received
            s.close()

        except Exception as e:
            SERVER_IP = None  # server is dead
            if START_UP == False and SERVER_IP == None and NEW_SERVER_IP == True:
                G_Socket_Poz.close()
                G_Socket_Poz = None
                print("Socket from get position closed!")

            #print("Not connected to server! IP: " + str(SERVER_IP) + "! Error:" + str(e))
            s.close()


# ------------------------------------------------------------------------
def SendIDToServer(new_server_ip):
    # sends THIS_CAR_ID to server for identification
    global SERVER_IP
    global CAR_SUBSCRITPION_PORT
    global THIS_CAR_ID
    global ID_SENT_FLAG

    try:
        s = socket()
        print("Vehicle " + str(THIS_CAR_ID) + " subscribing to GPS server: " + str(new_server_ip) + ":" + str(
            CAR_SUBSCRITPION_PORT))
        s.connect((new_server_ip, CAR_SUBSCRITPION_PORT))
        sleep(2)
        car_id_str = str(THIS_CAR_ID)
        s.send(bytes(car_id_str, "UTF-8"))
        s.shutdown(SHUT_WR)
        s.close()
        print("Vehicle ID sent to server----------------------------")
        ID_SENT_FLAG = True
    except Exception as e:
        print("Failed to send ID to server, with error: " + str(e))
        s.close()
        ID_SENT_FLAG = False


# ------------------------------------------------------------------------
def GetPositionDataFromServer_Thread():
    # receive car position and orientation from server
    global car_id
    global car_poz
    global car_or
    global SERVER_IP
    global NEW_SERVER_IP
    global THIS_CAR_ID
    global CAR_COMMUNICATION_PORT
    global G_Socket_Poz
    global RUN_CARCLIENT
    global changeEvent


    car_poz = 0.0 + 1.0j
    car_or = 0.0 + 0.0j

    while RUN_CARCLIENT:
        # fac acuma sa pornesc de la un punct hardcoded si sa tot adaug 1 pe x la 1 sec (sa merg paralel cu axa x)
        # momentan nu ma intereseaza orientarea masini
        '''sleep(1)
        car_poz = 1.0 + 2.0j
        changeEvent.set()
        sleep(1)
        car_poz = 1.0 - 2.0j
        changeEvent.set()
        '''

        if SERVER_IP != None:  # if server alive/valid
            if NEW_SERVER_IP == True:
                try:
                    G_Socket_Poz.close()
                    G_Socket_Poz = None
                except:
                    # do nothing
                    print("do nothing ...")
            if G_Socket_Poz == None:
                try:
                    # if there is a GPS server available then open a socket and then wait for GPS data
                    print(
                        "New socket was created to receive the position from the serves of:" + str(HOST_IP) + ":" + str(
                            CAR_COMMUNICATION_PORT))
                    G_Socket_Poz = socket()
                    G_Socket_Poz.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
                    G_Socket_Poz.bind((HOST_IP, CAR_COMMUNICATION_PORT))
                    
                    G_Socket_Poz.listen(2)
                    NEW_SERVER_IP = False
                except Exception as e:
                    print("Creating new socket for get position from server: " + str(SERVER_IP) + " with error: " + str(
                        e))
                    sleep(1)
                    G_Socket_Poz = None
            if not G_Socket_Poz == None:
                try:
                    c, addr = G_Socket_Poz.accept()
                    data = str(c.recv(4096))  # raw message
                    # mesage parsing
                    car_id = int(data.split(';')[0].split(':')[1])
                    if car_id == THIS_CAR_ID:
                        # aici se updateaza pozitia si orientarea masinii pe harta
                        car_poz_new = complex(float(data.split(';')[1].split(':')[1].split('(')[1].split('+')[0]),
                                          float(data.split(';')[1].split(':')[1].split('j')[0].split('+')[1]))
                        car_or_new = complex(float(data.split(';')[2].split(':')[1].split('(')[1].split('+')[0]),
                                         float(data.split(';')[2].split(':')[1].split('j')[0].split('+')[1]))
                        car_poz1 = complex(car_poz_new.real/10, car_poz_new.imag/10)
                        #print("id:" + str(car_id) + " -position: " + str(car_poz1) + " -orientation: " + str(car_or_new))
                        if car_poz_new != car_poz or car_or_new != car_or:
                            car_poz = car_poz_new
                            car_or = car_or_new
                            changeEvent.set()
        
                    c.close()
                except Exception as e:
                    print("Receiving position data from server failed! from: " + str(SERVER_IP) + " with error: " + str(e))
                    c.close()

                # [TO DO] validate received GPS data


'''
    deci pot face o clasa care are 2 atribute: pozitia si orientarea(private), si am gettere la ele (vreau acces sincronizat). 
    Acestea se vor modifica intern (sincronizat)
    Si asta va putea sa mosteneasca thread (cred ca asa e cel mai usor)
    in run rulez main-ul dat ca exemplu aici si trebuie sa modific threadul care primeste valorile sa le puna sincronizat in 
    atributele clasei

    deci, cand masina executa o curba, ea merge un pic, gps are un interval la care se updateaza
    daca pun un event si apelez in control o functie care asteapta dupa event iar eventul sa fie ca s-a modificat poz sau orientarea
'''
class CarInfo(Thread):
    def __init__(self):
        super(CarInfo, self).__init__()
        self.connection_thread = None
        self.position_thread = None

    def run(self):
        global RUN_CARCLIENT
        print("First start up of vehicle client ...")

        # thread responsible for subscribing to GPS server
        self.connection_thread = Thread(target=GetServer)

        # thread responsible for receiving GPS data from serverw
        self.position_thread = Thread(target=GetPositionDataFromServer_Thread)

        try:
            # start all threads
            self.connection_thread.start()
            self.position_thread.start()
            while RUN_CARCLIENT:
                print("Running")
                sleep(10)
        except KeyboardInterrupt as e:
            RUN_CARCLIENT = False
            try:
                global G_Socket_Poz
                G_Socket_Poz.close()
            except:
                print("Cannot close client socket!")
                pass
            print("KeyboardInterrupt!!")
            pass





    def getInfo(self):
        global car_poz
        global car_or
        global changeEvent

        global carHeight
        global carWidth

        changeEvent.wait()
        changeEvent.clear()

        car_poz1 = complex(car_poz.real/10, car_poz.imag/10)
        car_or1 = complex(-car_or.imag, car_or.real)

        carPos = [car_poz1.real, car_poz1.imag]
        carOr = [car_or1.real, car_or1.imag]

        carOr90Right = [carOr[1], -carOr[0]]

        heightVectorScale = [carOr[0] * carHeight, carOr[1] * carHeight]
        widthVectorScale = [carOr90Right[0] * carWidth, carOr90Right[1] * carWidth]

        carCenterPos = [carPos[0] + heightVectorScale[0] + widthVectorScale[0], carPos[1] + heightVectorScale[1] + widthVectorScale[1]]

        #print(car_poz1, car_or1)
        return (carCenterPos, carOr)

    def close(self):
        global RUN_CARCLIENT
        RUN_CARCLIENT = False

# ------------------------------------------------------------------------
def main():
    global RUN_CARCLIENT
    print("First start up of vehicle client ...")

    # thread responsible for subscribing to GPS server
    connection_thread = Thread(target=GetServer)

    # thread responsible for receiving GPS data from serverw
    position_thread = Thread(target=GetPositionDataFromServer_Thread)

    try:
        # start all threads
        connection_thread.start()
        position_thread.start()
        while RUN_CARCLIENT:
            print("Running")
            sleep(30)
    except KeyboardInterrupt as e:
        RUN_CARCLIENT = False
        try:
            global G_Socket_Poz
            G_Socket_Poz.close()
        except:
            print("Cannot close client socket!")
            pass
        print("KeyboardInterrupt!!")
        pass

def testCarInfo():
    carInfo = CarInfo()
    carInfo.start()
    (carPos, carOr) = carInfo.getInfo()
    print(carPos, carOr)
    sleep(2)
    (carPos, carOr) = carInfo.getInfo()
    print(carPos, carOr)
    carInfo.close()


if __name__ == "__main__":
    testCarInfo()
